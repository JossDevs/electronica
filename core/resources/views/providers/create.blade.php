@extends('app')
@section('content')
<section class="content">
   <form action="https://sistema.factexp.site/plataforma/transportistas/actualizar/1" method="post">
      <input type="hidden" name="_token" value="EFIbAZRw8Rc0zk9cbMLXFNcWUTHCOpXk5Rkf13Wz"> 
      <div class="box">
         <div class="box-body">
            <div class="col-md-12  ">
               <div class="form-group row">
                  <input type="hidden" id="id_transportista" value="1"> 
                  <div class="col-md-2"> <label class="form-control-label" for="l0">Tipo Identificacion</label> </div>
                  <div class="col-md-4">
                     <select class="form-control requerido" name="tipoidentificacion" id="tipoidentificacion">
                        <option selected="" value="05">Cedula</option>
                        <option value="04">Ruc</option>
                        <option value="06">Pasaporte</option>
                        <option value="08">Identificacion del Exterior</option>
                        <option value="09">Placa</option>
                     </select>
                  </div>
                  <div class="col-md-2"> <label class="form-control-label " for="l0">Identificacion</label> </div>
                  <div class="col-md-4"> <input name="transportista_identificacion" type="text" class="form-control requerido validador_solonumero" placeholder="9999999999999" id="transportista_identificacion" value="0930342878"> </div>
               </div>
            </div>
            <div class="col-md-12">
               <div class="form-group row">
                  <div class="col-md-2"> <label class="form-control-label" for="l0">Nombres/Razon Social</label> </div>
                  <div class="col-md-4"> <input name="transportista_nombrerazonsocial" type="text" class="form-control requerido mayuscula" placeholder="Consumidor Final" id="transportista_nombrerazonsocial" value="CONSUMER"> </div>
                  <div class="col-md-2"> <label class="form-control-label" for="l0">Dirección</label> </div>
                  <div class="col-md-4"> <input name="transportista_direccion" type="text" class="form-control requerido mayuscula" placeholder="N/D" id="transportista_direccion" value=""> </div>
               </div>
            </div>
            <div class="col-md-12">
               <div class="form-group row">
                  <div class="col-md-1"> <label class="form-control-label" for="l0">Telefono</label> </div>
                  <div class="col-md-2"> <input name="transportista_telefono" type="text" class="form-control requerido" placeholder="N/D" id="transportista_telefono" value=""> </div>
                  <div class="col-md-1"> <label class="form-control-label" for="l0">Celular</label> </div>
                  <div class="col-md-2"> <input name="transportista_celular" type="text" class="form-control" placeholder="N/D" id="transportista_celular" value=""> </div>
                  <div class="col-md-2"> <label class="form-control-label" for="l0">Correo</label> </div>
                  <div class="col-md-4"> <input name="transportista_correo" type="text" class="form-control requerido" placeholder="N/D" id="transportista_correo" value=""> </div>
               </div>
            </div>
            <div class="col-md-12  ">
               <div class="form-group row">
                  <div class="col-md-1"> <label class="form-control-label" for="l0">Provincia</label> </div>
                  <div class="col-md-2">
                     <select class="form-control" id="transportista_provincia" name="transportista_provincia">
                        <option value="1064">Azuay</option>
                        <option value="1061">Bolivar</option>
                        <option value="1063">Cañar</option>
                        <option value="1056">Carchi</option>
                        <option value="1062">Chimborazo</option>
                        <option value="1059">Cotopaxi</option>
                        <option value="1055">El Oro</option>
                        <option value="1053">Esmeraldas</option>
                        <option value="888">Exterior</option>
                        <option selected="true" value="1072">Galapagos</option>
                        <option value="1050">Guayas</option>
                        <option value="1057">Imbabura</option>
                        <option value="1065">Loja</option>
                        <option value="1051">Los Rios</option>
                        <option value="1052">Manabi</option>
                        <option value="1070">Morona Santiago</option>
                        <option value="1067">Napo</option>
                        <option value="1068">Orellana</option>
                        <option value="1069">Pastaza</option>
                        <option value="1058">Pichincha</option>
                        <option value="1054">Santa Elena</option>
                        <option value="1074">Santo Domingo</option>
                        <option value="999">Sin Especificar</option>
                        <option value="1066">Sucumbios</option>
                        <option value="1060">Tungurahua</option>
                        <option value="1071">Zamora Chinchipe</option>
                     </select>
                  </div>
                  <div class="col-md-1"> <label class="form-control-label" for="l0">Ciudad</label> </div>
                  <div class="col-md-2">
                     <select name="transportista_ciudad" class="form-control" id="transportista_ciudad">
                        <option selected="true" value="1003">Isabela</option>
                        <option value="1004">San Cristobal</option>
                        <option value="1005">Santa Cruz</option>
                     </select>
                  </div>
                  <div class="col-md-2"> <label class="form-control-label" for="l0">Placa</label> </div>
                  <div class="col-md-4"> <input name="transportista_placa" type="text" class="form-control requerido" placeholder="N/D" id="transportista_placa" value="xswe"> </div>
               </div>
            </div>
         </div>
         <div class="box-footer"> <button class="btn btn-success"><i class="fa fa-save"></i> Actualizar Transportista</button> </div>
      </div>
   </form>
   <script> $('#transportista_provincia').change(function(){ buscarciudadtransportista(); }) buscarciudadtransportista(); function buscarciudadtransportista(){ var provincia = $('#transportista_provincia').val(); $.ajax({ type: 'POST', url: "https://sistema.factexp.site/plataforma/ciudades", data: { id_provincia: provincia, }, success: function (resp) { if (resp.respuesta == true) { $('#transportista_ciudad').empty(); $(resp.datos).each(function (i, v) { if(v.id=="1003"){ $('#transportista_ciudad').append(' <option selected="true" value=' + v.id + '>' + v.nombre + '</option>'); }else{ $('#transportista_ciudad').append(' <option value=' + v.id + '>' + v.nombre + '</option>'); } }); } } }) } </script> 
</section>
@endsection

