<div class="content-wrapper" style="min-height: 740px;">


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h1>
            Guias en AZUR FACTURACIÓN ELECTRÓNICA Codigo Estab. 001



        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Azur Facturación Electronica </a>
            </li>
            <li class="active">Guias/Listado</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">







        <nav class="navbar navbar-default navbarmio">
            <div class="">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand visible-xs" href="#">Filtros de Búsqueda</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <form class="">
                        <input type="hidden" name="global" value="">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon">Búsqueda</span>
                                    <input class="form-control mr-sm-2" name="busqueda" type="search" placeholder="Ingrese su Búsqueda" aria-label="Search" value="">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon">Fecha Inicio</span>
                                    <input class="form-control" type="text" name="fecha_inicio" id="fechainicio" value="">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon">Fecha Fin</span>
                                    <input type="text" class="form-control" name="fecha_fin" id="fechafin" value="">

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon">Estado</span>
                                    <select class="form-control" name="estadocomprobante"><option value="0">Procesando</option><option value="1">Generado</option><option value="2">Firmado</option><option value="3">Recibido</option><option value="4">Autorizado</option><option value="5">No Autorizado</option><option value="6">Devuelto</option><option value="7">Error en Firmado</option><option value="8">Pendiente Autorización</option><option value="9">Anulado</option><option selected="true" value="99">Todos</option><option value="999">Eliminados</option></select>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon">Ordenar Por</span>
                                    <select class="form-control" name="ordenar_por"><option value="all">Defecto</option><option value="fecha">Fecha</option><option value="secuencial">Secuencial</option></select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon">Orden</span>
                                    <select class="form-control" name="orden"><option value="asc">Ascendente</option><option value="desc">Descendente</option></select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon">Ver</span>
                                    <select class="form-control" name="paginacion"><option selected="true" value="0">Defecto</option><option value="5">5</option><option value="10">10</option><option value="20">20</option><option value="50">50</option><option value="100">100</option><option value="all">Todos</option></select>
                                </div>
                            </div>

                        </div>


                        <div class="row">
                            <div class=" col-md-4">
                                <button class="btn bg-maroon btn-flat btn-block" type="submit">
                                    Aplicar Filtros
                                </button>
                            </div>

                            <div class=" col-md-4">
                                <button class="btn bg-olive btn-flat btn-block" name="accion" value="excel" type="submit">
                                    Descargar en Excel
                                </button>
                            </div>
                        </div>
                    </form>

                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

        <!-- /.box -->

        <div class="box box-primary">

            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover ">
                        <thead class="thead-dark">
                        <tr>
                            <th><i class="icmn-key2 opc" aria-hidden="true"></i></th>
                            <th>Fecha Emisión</th>
                            <th>Estab.</th>
                            <th>PtoEmi.</th>
                            <th>Secuencial</th>
                            <th>Cliente</th>
                            <th>Transportista</th>
                            <th>Ambiente</th>
                            <th>Vendedor</th>
                            <th>Estado</th>
                            <th>Tipo</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <!-- Previous Page Link -->
                        <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1">«</a>
                        </li>

                        <!-- Pagination Elements -->
                        <!-- "Three Dots" Separator -->

                        <!-- Array Of Links -->
                        <li class="page-item active">
                            <a class="page-link" href="#">
                                <span class="">1</span>
                            </a>
                        </li>

                        <!-- Next Page Link -->
                        <li class="disabled page-item"><span>»</span></li>
                    </ul>
                </nav>
            </div>
            <!-- box-footer -->
        </div>
        <!-- /.box -->






        <script>


            $(function () {

                $('#fechainicio').datetimepicker({
                    widgetPositioning: {
                        horizontal: 'left'
                    },
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    },
                    format: 'YYYY-MM-DD'
                });

                $('#fechafin').datetimepicker({

                    widgetPositioning: {
                        horizontal: 'left'
                    },
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    },
                    format: 'YYYY-MM-DD'
                });
            })

        </script>
    </section>
    <!-- /.content -->

</div>