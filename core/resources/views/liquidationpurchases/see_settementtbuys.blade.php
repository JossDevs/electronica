<div class="content-wrapper" style="min-height: 740px;">


    <!-- Content Header (Page header) -->
    <section class="content-header">

        <h1>
            Compras en AZUR FACTURACIÓN ELECTRÓNICA Codigo Estab. 001

        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Azur Facturación Electronica </a>
            </li>
            <li class="active">Compra/Listado</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">




        <nav class="navbar navbar-default navbarmio">
            <div class="">

                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand visible-xs" href="#">Filtros de Búsqueda</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <form class="">
                        <input type="hidden" name="global" value="">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon">Búsqueda</span>
                                    <input class="form-control mr-sm-2" name="busqueda" type="search" placeholder="Ingrese su Búsqueda" aria-label="Search" value="">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon">Fecha Inicio</span>
                                    <input class="form-control" type="text" name="fecha_inicio" id="fechainicio" value="">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon">Fecha Fin</span>
                                    <input type="text" class="form-control" name="fecha_fin" id="fechafin" value="">

                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon">Ordenar Por</span>
                                    <select class="form-control" name="ordenar_por"><option value="all">Defecto</option><option value="fecha">Fecha</option><option value="secuencial">Secuencial</option></select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon">Orden</span>
                                    <select class="form-control" name="orden"><option value="asc">Ascendente</option><option value="desc">Descendente</option></select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon">Ver</span>
                                    <select class="form-control" name="paginacion"><option selected="true" value="0">Defecto</option><option value="5">5</option><option value="10">10</option><option value="20">20</option><option value="50">50</option><option value="100">100</option><option value="all">Todos</option></select>
                                </div>
                            </div>

                        </div>


                        <div class="row">
                            <div class=" col-md-4">
                                <button class="btn bg-maroon btn-flat btn-block" name="accion" value="filtrar" type="submit">
                                    Aplicar Filtros
                                </button>
                            </div>

                            <div class=" col-md-4">
                                <button class="btn bg-olive btn-flat btn-block" name="accion" value="excel" type="submit">
                                    Descargar en Excel
                                </button>
                            </div>
                        </div>
                    </form>

                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

        <!-- /.box -->

        <div class="box box-primary">

            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover ">
                        <thead class="thead-dark">
                        <tr>
                            <th><i class="icmn-key2 opc" aria-hidden="true"></i></th>
                            <th>Fecha Emisión</th>
                            <th>Estab-PtoEmi</th>
                            <th>Secuencial</th>
                            <th>Tipo</th>
                            <th>Cliente</th>
                            <th>Ambiente</th>
                            <th>Vendedor</th>
                            <th>Subtotal</th>
                            <th>ICE</th>
                            <th>IVA</th>
                            <th>Total</th>
                            <th>Estado</th>

                            <th>Pago</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody><tr>
                            <td><i data-toggle="tooltip" data-placement="right" data-original-title="156" class="icmn-key2 opc" aria-hidden="true"></i></td>
                            <td>2021-02-18</td>
                            <td>002
                                -001</td>
                            <td>000016078
                            </td><td>FACTURA</td>

                            <td>1-700 Digital S.A.</td>
                            <td>PRUEBAS</td>
                            <td>Demo</td>
                            <th class="estilovalor">10.000</th>
                            <th class="estilovalor">0.000</th>
                            <th class="estilovalor">1.200</th>
                            <th class="estilovalor">11.20</th>
                            <td>
                                Activo
                            </td>

                            <td>
                                Pagado
                            </td>


                            <td>
                                <a onclick="verpagocxp('156','01');">
                                    <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos" aria-hidden="true"></i>
                                </a>
                                <a href="https://azur.com.ec/plataforma/compra/imprimir/normal/empresa/1/id/156?tipo_comprobante=01&amp;tipo_tecnologia=0" data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                    <i class="icmn-printer2 opc" aria-hidden="true"></i>
                                </a>





                                <a class="eliminarcomprobante" claveacceso="156" id_usuario="1" tipo_comprobante="compra" data-toggle="tooltip" data-placement="top" data-original-title="Eliminar Compra" aria-describedby="tooltip359635">
                                    <i class="fa fa-trash opc" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>

                        </tbody></table>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <!-- Previous Page Link -->
                        <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1">«</a>
                        </li>

                        <!-- Pagination Elements -->
                        <!-- "Three Dots" Separator -->

                        <!-- Array Of Links -->
                        <li class="page-item active">
                            <a class="page-link" href="#">
                                <span class="">1</span>
                            </a>
                        </li>

                        <!-- Next Page Link -->
                        <li class="disabled page-item"><span>»</span></li>
                    </ul>
                </nav>
            </div>
            <!-- box-footer -->
        </div>
        <!-- /.box -->


        <div class="modal fade in" id="modal-pagos">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">
                            Pagos
                            <span id="id_texto_pago"></span>

                            <button onclick="nuevopago()" type="button" data-toggle="tooltip" data-original-title="Nuevo Pago" aria-hidden="true" class="btn btn-primary ">
                                <i class="fa fa-money "></i> &nbsp;Nuevo Pago
                            </button>


                            <button onclick="nuevaretencion()" type="button" data-toggle="tooltip" data-original-title="Nueva Retención Recibida" aria-hidden="true" class="btn btn-primary ">
                                <i class="fa fa-money "></i> &nbsp;Nueva Retención Recibida
                            </button>

                        </h4>

                    </div>
                    <div class="modal-body " id="animacionpagos">

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover ">
                                <thead class="thead-dark">
                                <tr><td>Id</td>
                                    <td>Fecha</td>
                                    <td>Fecha Vencimiento</td>
                                    <td>Concepto</td>
                                    <td>Débito</td>
                                    <td>Crédito</td>
                                    <td>X</td>
                                </tr></thead>
                                <tbody id="id_tabla_pagos">

                                </tbody>
                                <tfoot id="id_tabla_pagosfooter">

                                </tfoot>
                            </table>
                        </div>

                        <button type="button" class="btn btn-danger " data-dismiss="modal">Cerrar</button>
                    </div>


                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div class="modal fade in" id="modal-nuevopago">
            <div class="modal-dialog modal-xs">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">
                            Nuevo Pago
                        </h4>

                    </div>
                    <div class="modal-body " id="animacionpagos">
                        <form id="formulariopago" enctype="multipart/form-data">
                            <input type="hidden" id="pago_id_comprobante">
                            <input type="hidden" id="pago_tipo_comprobante">

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        Fecha
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="pago_fecha" value="2021-04-19">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        Concepto
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" id="pago_concepto">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        Forma de Pago
                                        <small>(Opcional)</small>
                                    </div>
                                    <div class="col-md-9">
                                        <select id="pago_formapago" class="form-control" name="pago_formapago">
                                            <option value="01">Sin utilizacion del sistema financiero</option>
                                            <option value="16">Tarjetas de Debito</option>
                                            <option value="17">Dinero Electronico</option>
                                            <option value="18">Tarjeta Prepago</option>
                                            <option value="19">Tarjeta de Credito</option>
                                            <option selected="" value="20">Otros con Utilizacion del Sistema Financiero</option>
                                            <option value="21">Endoso de Titulos</option>
                                            <option value="15">COMPENSACIÓN DE DEUDAS</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        Valor
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" id="pago_valor" class="form-control validador_numero2 usd2">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        Observacion
                                        <small>(Opcional)</small>
                                    </div>
                                    <div class="col-md-9">
                                        <textarea type="text" class="form-control" id="pago_observacion"></textarea>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <button type="button" class="btn btn-danger " data-dismiss="modal">Cerrar</button>
                        <button onclick="guardarpagocxp()" type="button" data-toggle="tooltip" data-original-title="Guardar Pago" aria-hidden="true" class="btn btn-primary pull-right">
                            <i class="fa fa-money "></i> &nbsp;Guardar
                        </button>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->




        <div class="modal fade in" id="modal-fotopago">
            <div class="modal-dialog modal-xs">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">
                            Foto
                        </h4>

                    </div>
                    <div class="modal-body ">
                        <img id="cargafoto" src="" style="width: 100%; height: 100%" alt="">
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-dialog -->

        <script>

            function guardarpagocxp() {
                var id_comprobante = $("#pago_id_comprobante").val();
                var paqueteDeDatos = new FormData();
                paqueteDeDatos.append('id_comprobante', $("#pago_id_comprobante").val());
                paqueteDeDatos.append('tipo_comprobante', $("#pago_tipo_comprobante").val());
                paqueteDeDatos.append('pago_fecha', $("#pago_fecha").val());
                paqueteDeDatos.append('pago_concepto', $("#pago_concepto").val());
                paqueteDeDatos.append('pago_formapago', $("#pago_formapago").val());
                paqueteDeDatos.append('pago_valor', $("#pago_valor").val());
                paqueteDeDatos.append('pago_observacion', $("#pago_observacion").val());
                paqueteDeDatos.append('api_key2', "API_1_2_5a4492f2d5137");

                swal({
                        title: "Esta Seguro que desea Guardar el Pago",
                        text: "Guardar Pago",
                        type: "info",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true,
                    },
                    function () {


                        $.ajax({
                            type: 'POST',
                            url: "https://azur.com.ec/plataforma/guardarpagosdecxp",
                            contentType: false,
                            processData: false,
                            cache: false,
                            data: paqueteDeDatos,
                            success: function (resp) {

                                if (resp.respuesta == true) {
                                    swal({
                                        title: "Excelente!",
                                        text: "Guardado",
                                        type: "success",
                                        showConfirmButton: false,
                                        timer: 1000
                                    });
                                    $('#modal-nuevopago').modal('hide');
                                    verpagocxp(resp.id_comprobante,resp.tipo_comprobante);

                                } else {
                                    swal({
                                        title: "ERROR!",
                                        text: "Error en el proceso Vuelva a intentarlo",
                                        type: "error",
                                        showConfirmButton: false,
                                        timer: 1000
                                    });
                                }
                            },error : function(xhr) {
                                erroresenajax(xhr);
                            },
                        });
                    });


            }

            function nuevopago() {
                $('#modal-nuevopago').modal('show');
            }

            function verpagocxp(id_comprobante,tipo_comprobante) {


                $('#id_tabla_pagos').empty();
                $('#pago_id_comprobante').val("");
                $('#pago_tipo_comprobante').val("");
                $('#modal-pagos').modal('show');
                $("#animacionpagos").LoadingOverlay("show");
                $('#pago_id_comprobante').val(id_comprobante);
                $('#pago_tipo_comprobante').val(tipo_comprobante);

                var auxtexto="";
                if(tipo_comprobante=="01" || tipo_comprobante=="1"){
                    auxtexto="Factura";
                }else if(tipo_comprobante=="03" || tipo_comprobante=="3"){
                    auxtexto="Liquidacion de Compra";
                }else if(tipo_comprobante=="05" || tipo_comprobante=="5") {
                    auxtexto = "Nota de Debito";
                }else{
                    auxtexto="otro";
                }

                $.ajax({
                    type: 'POST',
                    url: "https://azur.com.ec/plataforma/pagosdecomprobantecxp",
                    data: {
                        id_comprobante: id_comprobante,
                        tipo_comprobante:tipo_comprobante,
                        api_key2: "API_1_2_5a4492f2d5137"
                    },
                    success: function (resp) {
                        if (resp.respuesta == true) {
                            $('#id_texto_pago').html(auxtexto+" # " + resp.numero);
                            $('#id_tabla_pagos').empty();
                            $('#id_tabla_pagosfooter').empty();
                            $("#pago_concepto").val("");
                            $("#pago_valor").val("");
                            $(resp.transacciones).each(function (i, v) {
                                var debito = 0;
                                var credito = 0;
                                if (v.tipo == 2) {
                                    credito = v.valor;
                                    debito = 0;
                                } else if (v.tipo == 1) {
                                    credito = 0;
                                    debito = v.valor;
                                }

                                var botoneliminar="";

                                if(v.tipo == 2){
                                    botoneliminar='<a id="eliminarpagocxp" id_pago="' + v.id + '"><i class="fa fa-times-circle anadir"></i></a>';
                                }else if( v.tipo == 1){
                                    botoneliminar='';
                                }

                                if (v.observacion == "" || v.observacion == null) {
                                    var observacion = '';
                                } else if (v.observacion != "") {
                                    var observacion =  v.observacion;
                                }

                                $("#id_tabla_pagos").append('<tr>' +
                                    '<td>' + v.id + '</td>' +
                                    '<td>' + v.fecha + '</td>' +
                                    '<td>' + v.vencimiento + '</td>' +
                                    '<td>' + v.concepto +'<br>'+observacion+ '</td>' +
                                    '<td style="text-align: right">' + debito + '</td>' +
                                    '<td style="text-align: right">' + credito + '</td>' +
                                    '<td>'+botoneliminar+'</td>'+
                                    '</tr>');
                            })
                            $("#id_tabla_pagosfooter").append('<tr>' +
                                '<td colspan="4"></td>' +
                                '<td><b>Saldo</b></td>' +
                                '<td style="text-align: right">' + resp.saldo + '</td>' +
                                '</tr>');

                            $("#pago_concepto").val("Pago "+auxtexto+" # " + resp.numero);
                            $("#pago_valor").val(resp.saldo);
                            $("#animacionpagos").LoadingOverlay("hide");

                        } else {
                            $("#animacionpagos").LoadingOverlay("hide");

                        }
                    }
                })
            }


            function eliminarpagocxp(id_pago) {
                $.ajax({
                    type: 'POST',
                    url: "https://azur.com.ec/plataforma/eliminarpagosdecxp",
                    data: {
                        id_pago: id_pago,
                        api_key2: "API_1_2_5a4492f2d5137"
                    },
                    success: function (resp) {
                        if (resp.respuesta == true) {
                            if(resp.id_comprobante!=""){
                                verpagocxp(resp.id_comprobante,resp.tipo_comprobante)
                            }
                            swal({
                                title: "Excelente!",
                                text: "Pago Eliminado",
                                type: "success",
                                showConfirmButton: false,
                                timer: 1000
                            });

                        } else {
                            swal({
                                title: "ERROR!",
                                text: "Error en el proceso Vuelva a intentarlo",
                                type: "error",
                                showConfirmButton: false,
                                timer: 1000
                            });
                        }
                    },error : function(xhr) {
                        erroresenajax(xhr);
                    },
                })
            }

            $(function () {
                $('#pago_fecha').datetimepicker({

                    widgetPositioning: {
                        horizontal: 'left'
                    },
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    },
                    format: 'YYYY-MM-DD'
                });
            });

            $('#id_tabla_pagos').on('click', '#eliminarpagocxp', function () {
                var id_pago = this.getAttribute("id_pago");
                var fila = $(this).parents('tr');
                fila.remove();
                eliminarpagocxp(id_pago);
            });

            function nuevaretencion() {
                var id_compra=$("#pago_id_comprobante").val();
                window.location.href ='https://www.azur.com.ec/plataforma/retencion/nuevaretencion_lleno_compra/'+id_compra+'/';
            }


        </script>
        <script>


            $(function () {

                $('#fechainicio').datetimepicker({
                    widgetPositioning: {
                        horizontal: 'left'
                    },
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    },
                    format: 'YYYY-MM-DD'
                });

                $('#fechafin').datetimepicker({

                    widgetPositioning: {
                        horizontal: 'left'
                    },
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    },
                    format: 'YYYY-MM-DD'
                });

            })


            $('.eliminarcomprobante').click(function (e) {
                e.preventDefault();
                var claveacceso = this.getAttribute("claveacceso");
                var id_usuario = this.getAttribute("id_usuario");
                var tipo_comprobante = this.getAttribute("tipo_comprobante");
                swal({
                        title: "Esta seguro que desea eliminar este comprobante ?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Si, eliminar!",
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true
                    },
                    function () {

                        $.ajax({
                            type: 'POST',
                            url: 'https://azur.com.ec/plataforma/eliminarcomprobante',
                            data: {
                                claveacceso: claveacceso,
                                id_usuario: id_usuario,
                                tipo_comprobante: tipo_comprobante,
                            },
                            success: function (resp) {
                                if (resp.respuesta == true) {
                                    swal({
                                        type: 'success',
                                        title: 'Eliminado',
                                        showConfirmButton: true,
                                        timer: 2500
                                    });
                                    location.href = "https://azur.com.ec/plataforma/compra/vertodos";
                                } else {
                                    swal({
                                        type: 'error',
                                        title: 'Error',
                                        text: resp.error,
                                        showConfirmButton: true
                                    });
                                    console.log(resp);
                                }
                            }
                        })


                    });
            });


        </script>
    </section>
    <!-- /.content -->

</div>
