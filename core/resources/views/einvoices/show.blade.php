@extends('app')
@section('content')

    <!-- Main content -->
    <section class="content">

        <nav class="navbar navbar-default navbarmio">
            <div class="">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed " data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand visible-xs" href="#">Filtros de Búsqueda</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <form class="">
                        <input type="hidden" name="global" value="" >
                        <div class="row">
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon">Búsqueda</span>
                                    <input class="form-control mr-sm-2" name="busqueda" type="search"
                                           placeholder="Ingrese su Búsqueda" aria-label="Search"
                                           value="">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon">Fecha Inicio</span>
                                    <input class="form-control" type="text" name="fecha_inicio" id="fechainicio"
                                           value="">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon">Fecha Fin</span>
                                    <input type="text" class="form-control" name="fecha_fin" id="fechafin"
                                           value="">

                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon">Estado</span>
                                    <select class='form-control' name='estadocomprobante'><option value='0'>Procesando</option><option value='1'>Generado</option><option value='2'>Firmado</option><option value='3'>Recibido</option><option value='4'>Autorizado</option><option value='5'>No Autorizado</option><option value='6'>Devuelto</option><option value='7'>Error en Firmado</option><option value='8'>Pendiente Autorización</option><option value='9'>Anulado</option><option selected='true' value='99'>Todos</option><option value='999'>Eliminados</option></select>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon">Ordenar Por</span>
                                    <select class='form-control' name='ordenar_por'><option value='all'>Defecto</option><option value='fecha'>Fecha</option><option value='secuencial'>Secuencial</option></select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon">Orden</span>
                                    <select class='form-control' name='orden'><option value='asc'>Ascendente</option><option value='desc'>Descendente</option></select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <span class="input-group-addon">Ver</span>
                                    <select class='form-control' name='paginacion'><option selected='true' value='0'>Defecto</option><option value='5'>5</option><option value='10'>10</option><option value='20'>20</option><option value='50'>50</option><option value='100'>100</option><option value='all'>Todos</option></select>
                                </div>
                            </div>

                        </div>


                        <div class="row">
                            <div class=" col-md-4">
                                <button class="btn bg-maroon btn-flat btn-block" name="accion" value="filtrar"
                                        type="submit">
                                    Aplicar Filtros
                                </button>
                            </div>

                        </div>
                    </form>

                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

        <!-- /.box -->

        <div class="box box-primary">

            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover ">
                        <thead class="thead-dark">
                        <tr>
                            <th><i class="icmn-key2 opc" aria-hidden="true"></i></th>
                            <th>Fecha Emisión</th>
                            <th>Estab-PtoEmi</th>
                            <th>Secuencial</th>
                            <th>Cliente</th>
                            <th>Ambiente</th>
                            <th>Vendedor</th>
                            <th>Subtotal</th>
                            <th>ICE</th>
                            <th>IVA</th>
                            <th>Total</th>
                            <th>Estado</th>

                            <th>Pago</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tr                                                              >
                            <td><i data-toggle="tooltip" data-placement="right"
                                   data-original-title="5521" class="icmn-key2 opc"
                                   aria-hidden="true"></i></td>
                            <td>2021-04-19</td>
                            <td>001-002</td>
                            <td>000000850



                            </td>
                            <td>AGENMIL - LA MONEDA AGENC...</td>
                            <td>PRUEBAS</td>
                            <td>Demo</td>
                            <th class="estilovalor">
                                <div data-toggle="tooltip" data-html="true"
                                     title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                    10.150
                                </div>
                            </th>

                            <th class="estilovalor">0.000</th>
                            <th class="estilovalor">1.220</th>
                            <th class="estilovalor">11.37</th>
                            <td>

                                Autorizado
                            </td>

                            <td>
                                Pendiente
                            </td>


                            <td>
                                <a onclick="verpagocxc('5521','01');">
                                    <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos"
                                       aria-hidden="true"></i>
                                </a>




                                <a href="https://azur.com.ec/plataforma/guia/nueva?id_factura=5521"
                                   data-toggle="tooltip" data-placement="top"
                                   data-original-title="Crear Guia">
                                    <i class="fa fa-truck opc" aria-hidden="true"></i>
                                </a>

                                <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5521"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                    <i class="icmn-printer2 opc" aria-hidden="true"></i>
                                </a>
                                <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5521"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                    <i class="icmn-printer opc" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>

                        <tr                                                              >
                            <td><i data-toggle="tooltip" data-placement="right"
                                   data-original-title="5504" class="icmn-key2 opc"
                                   aria-hidden="true"></i></td>
                            <td>2021-04-12</td>
                            <td>001-002</td>
                            <td>000000849



                            </td>
                            <td>AGENCIA DE VIAJES Y TURIS...</td>
                            <td>PRUEBAS</td>
                            <td>Demo</td>
                            <th class="estilovalor">
                                <div data-toggle="tooltip" data-html="true"
                                     title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                    9.500
                                </div>
                            </th>

                            <th class="estilovalor">0.000</th>
                            <th class="estilovalor">1.140</th>
                            <th class="estilovalor">10.64</th>
                            <td>

                                Autorizado
                            </td>

                            <td>
                                Pagado
                            </td>


                            <td>
                                <a onclick="verpagocxc('5504','01');">
                                    <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos"
                                       aria-hidden="true"></i>
                                </a>




                                <a href="https://azur.com.ec/plataforma/guia/nueva?id_factura=5504"
                                   data-toggle="tooltip" data-placement="top"
                                   data-original-title="Crear Guia">
                                    <i class="fa fa-truck opc" aria-hidden="true"></i>
                                </a>

                                <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5504"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                    <i class="icmn-printer2 opc" aria-hidden="true"></i>
                                </a>
                                <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5504"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                    <i class="icmn-printer opc" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>

                        <tr                                                              >
                            <td><i data-toggle="tooltip" data-placement="right"
                                   data-original-title="5478" class="icmn-key2 opc"
                                   aria-hidden="true"></i></td>
                            <td>2021-04-10</td>
                            <td>001-002</td>
                            <td>000000823



                            </td>
                            <td>AGENSITUR S.A.</td>
                            <td>PRUEBAS</td>
                            <td>Demo</td>
                            <th class="estilovalor">
                                <div data-toggle="tooltip" data-html="true"
                                     title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                    9.500
                                </div>
                            </th>

                            <th class="estilovalor">0.000</th>
                            <th class="estilovalor">1.140</th>
                            <th class="estilovalor">10.64</th>
                            <td>

                                Autorizado
                            </td>

                            <td>
                                Pendiente
                            </td>


                            <td>
                                <a onclick="verpagocxc('5478','01');">
                                    <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos"
                                       aria-hidden="true"></i>
                                </a>




                                <a href="https://azur.com.ec/plataforma/guia/nueva?id_factura=5478"
                                   data-toggle="tooltip" data-placement="top"
                                   data-original-title="Crear Guia">
                                    <i class="fa fa-truck opc" aria-hidden="true"></i>
                                </a>

                                <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5478"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                    <i class="icmn-printer2 opc" aria-hidden="true"></i>
                                </a>
                                <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5478"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                    <i class="icmn-printer opc" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>

                        <tr                                                              >
                            <td><i data-toggle="tooltip" data-placement="right"
                                   data-original-title="5479" class="icmn-key2 opc"
                                   aria-hidden="true"></i></td>
                            <td>2021-04-10</td>
                            <td>001-002</td>
                            <td>000000824



                            </td>
                            <td>AGENCIA DE VIAJES GARDNER...</td>
                            <td>PRUEBAS</td>
                            <td>Demo</td>
                            <th class="estilovalor">
                                <div data-toggle="tooltip" data-html="true"
                                     title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                    9.500
                                </div>
                            </th>

                            <th class="estilovalor">0.000</th>
                            <th class="estilovalor">1.140</th>
                            <th class="estilovalor">10.64</th>
                            <td>

                                Autorizado
                            </td>

                            <td>
                                Pendiente
                            </td>


                            <td>
                                <a onclick="verpagocxc('5479','01');">
                                    <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos"
                                       aria-hidden="true"></i>
                                </a>




                                <a href="https://azur.com.ec/plataforma/guia/nueva?id_factura=5479"
                                   data-toggle="tooltip" data-placement="top"
                                   data-original-title="Crear Guia">
                                    <i class="fa fa-truck opc" aria-hidden="true"></i>
                                </a>

                                <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5479"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                    <i class="icmn-printer2 opc" aria-hidden="true"></i>
                                </a>
                                <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5479"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                    <i class="icmn-printer opc" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>

                        <tr                                                         class="tr_error"
                        >
                            <td><i data-toggle="tooltip" data-placement="right"
                                   data-original-title="5485" class="icmn-key2 opc"
                                   aria-hidden="true"></i></td>
                            <td>2021-04-10</td>
                            <td>001-002</td>
                            <td>000000830



                            </td>
                            <td>AGENCIA DE VIAJES G-1 C....</td>
                            <td>PRUEBAS</td>
                            <td>Demo</td>
                            <th class="estilovalor">
                                <div data-toggle="tooltip" data-html="true"
                                     title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                    9.500
                                </div>
                            </th>

                            <th class="estilovalor">0.000</th>
                            <th class="estilovalor">1.140</th>
                            <th class="estilovalor">10.64</th>
                            <td>

                                No Autorizado
                            </td>

                            <td>
                                Pendiente
                            </td>


                            <td>
                                <a onclick="verpagocxc('5485','01');">
                                    <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos"
                                       aria-hidden="true"></i>
                                </a>





                                <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5485"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                    <i class="icmn-printer2 opc" aria-hidden="true"></i>
                                </a>
                                <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5485"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                    <i class="icmn-printer opc" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>

                        <tr                                                         class="tr_error"
                        >
                            <td><i data-toggle="tooltip" data-placement="right"
                                   data-original-title="5486" class="icmn-key2 opc"
                                   aria-hidden="true"></i></td>
                            <td>2021-04-10</td>
                            <td>001-002</td>
                            <td>000000831



                            </td>
                            <td>AGENCIA DE VIAJES G-1 C....</td>
                            <td>PRUEBAS</td>
                            <td>Demo</td>
                            <th class="estilovalor">
                                <div data-toggle="tooltip" data-html="true"
                                     title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                    9.500
                                </div>
                            </th>

                            <th class="estilovalor">0.000</th>
                            <th class="estilovalor">1.140</th>
                            <th class="estilovalor">10.64</th>
                            <td>

                                No Autorizado
                            </td>

                            <td>
                                Pendiente
                            </td>


                            <td>
                                <a onclick="verpagocxc('5486','01');">
                                    <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos"
                                       aria-hidden="true"></i>
                                </a>





                                <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5486"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                    <i class="icmn-printer2 opc" aria-hidden="true"></i>
                                </a>
                                <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5486"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                    <i class="icmn-printer opc" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>

                        <tr                                                         class="tr_error"
                        >
                            <td><i data-toggle="tooltip" data-placement="right"
                                   data-original-title="5487" class="icmn-key2 opc"
                                   aria-hidden="true"></i></td>
                            <td>2021-04-10</td>
                            <td>001-002</td>
                            <td>000000832



                            </td>
                            <td>ALBATUR SA</td>
                            <td>PRUEBAS</td>
                            <td>Demo</td>
                            <th class="estilovalor">
                                <div data-toggle="tooltip" data-html="true"
                                     title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                    9.500
                                </div>
                            </th>

                            <th class="estilovalor">0.000</th>
                            <th class="estilovalor">1.140</th>
                            <th class="estilovalor">10.64</th>
                            <td>

                                No Autorizado
                            </td>

                            <td>
                                Pendiente
                            </td>


                            <td>
                                <a onclick="verpagocxc('5487','01');">
                                    <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos"
                                       aria-hidden="true"></i>
                                </a>





                                <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5487"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                    <i class="icmn-printer2 opc" aria-hidden="true"></i>
                                </a>
                                <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5487"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                    <i class="icmn-printer opc" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>

                        <tr                                                         class="tr_error"
                        >
                            <td><i data-toggle="tooltip" data-placement="right"
                                   data-original-title="5488" class="icmn-key2 opc"
                                   aria-hidden="true"></i></td>
                            <td>2021-04-10</td>
                            <td>001-002</td>
                            <td>000000833



                            </td>
                            <td>AGENCIA DE VIAJES GARDNER...</td>
                            <td>PRUEBAS</td>
                            <td>Demo</td>
                            <th class="estilovalor">
                                <div data-toggle="tooltip" data-html="true"
                                     title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                    9.500
                                </div>
                            </th>

                            <th class="estilovalor">0.000</th>
                            <th class="estilovalor">1.140</th>
                            <th class="estilovalor">10.64</th>
                            <td>

                                No Autorizado
                            </td>

                            <td>
                                Pendiente
                            </td>


                            <td>
                                <a onclick="verpagocxc('5488','01');">
                                    <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos"
                                       aria-hidden="true"></i>
                                </a>





                                <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5488"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                    <i class="icmn-printer2 opc" aria-hidden="true"></i>
                                </a>
                                <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5488"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                    <i class="icmn-printer opc" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>

                        <tr                                                         class="tr_error"
                        >
                            <td><i data-toggle="tooltip" data-placement="right"
                                   data-original-title="5489" class="icmn-key2 opc"
                                   aria-hidden="true"></i></td>
                            <td>2021-04-10</td>
                            <td>001-002</td>
                            <td>000000834



                            </td>
                            <td>AGUIMAR C LTDA AGUITOUR</td>
                            <td>PRUEBAS</td>
                            <td>Demo</td>
                            <th class="estilovalor">
                                <div data-toggle="tooltip" data-html="true"
                                     title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                    9.500
                                </div>
                            </th>

                            <th class="estilovalor">0.000</th>
                            <th class="estilovalor">1.140</th>
                            <th class="estilovalor">10.64</th>
                            <td>

                                No Autorizado
                            </td>

                            <td>
                                Pendiente
                            </td>


                            <td>
                                <a onclick="verpagocxc('5489','01');">
                                    <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos"
                                       aria-hidden="true"></i>
                                </a>





                                <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5489"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                    <i class="icmn-printer2 opc" aria-hidden="true"></i>
                                </a>
                                <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5489"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                    <i class="icmn-printer opc" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>

                        <tr                                                         class="tr_error"
                        >
                            <td><i data-toggle="tooltip" data-placement="right"
                                   data-original-title="5490" class="icmn-key2 opc"
                                   aria-hidden="true"></i></td>
                            <td>2021-04-10</td>
                            <td>001-002</td>
                            <td>000000835



                            </td>
                            <td>CONSUMIDOR FINAL</td>
                            <td>PRUEBAS</td>
                            <td>Demo</td>
                            <th class="estilovalor">
                                <div data-toggle="tooltip" data-html="true"
                                     title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                    9.500
                                </div>
                            </th>

                            <th class="estilovalor">0.000</th>
                            <th class="estilovalor">1.140</th>
                            <th class="estilovalor">10.64</th>
                            <td>

                                No Autorizado
                            </td>

                            <td>
                                Pendiente
                            </td>


                            <td>
                                <a onclick="verpagocxc('5490','01');">
                                    <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos"
                                       aria-hidden="true"></i>
                                </a>





                                <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5490"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                    <i class="icmn-printer2 opc" aria-hidden="true"></i>
                                </a>
                                <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5490"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                    <i class="icmn-printer opc" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>

                        <tr                                                         class="tr_error"
                        >
                            <td><i data-toggle="tooltip" data-placement="right"
                                   data-original-title="5491" class="icmn-key2 opc"
                                   aria-hidden="true"></i></td>
                            <td>2021-04-10</td>
                            <td>001-002</td>
                            <td>000000836



                            </td>
                            <td>CONSUMIDOR FINAL</td>
                            <td>PRUEBAS</td>
                            <td>Demo</td>
                            <th class="estilovalor">
                                <div data-toggle="tooltip" data-html="true"
                                     title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                    9.500
                                </div>
                            </th>

                            <th class="estilovalor">0.000</th>
                            <th class="estilovalor">1.140</th>
                            <th class="estilovalor">10.64</th>
                            <td>

                                No Autorizado
                            </td>

                            <td>
                                Pendiente
                            </td>


                            <td>
                                <a onclick="verpagocxc('5491','01');">
                                    <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos"
                                       aria-hidden="true"></i>
                                </a>





                                <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5491"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                    <i class="icmn-printer2 opc" aria-hidden="true"></i>
                                </a>
                                <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5491"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                    <i class="icmn-printer opc" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>

                        <tr                                                         class="tr_error"
                        >
                            <td><i data-toggle="tooltip" data-placement="right"
                                   data-original-title="5492" class="icmn-key2 opc"
                                   aria-hidden="true"></i></td>
                            <td>2021-04-10</td>
                            <td>001-002</td>
                            <td>000000837



                            </td>
                            <td>CONSUMIDOR FINAL</td>
                            <td>PRUEBAS</td>
                            <td>Demo</td>
                            <th class="estilovalor">
                                <div data-toggle="tooltip" data-html="true"
                                     title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                    9.500
                                </div>
                            </th>

                            <th class="estilovalor">0.000</th>
                            <th class="estilovalor">1.140</th>
                            <th class="estilovalor">10.64</th>
                            <td>

                                No Autorizado
                            </td>

                            <td>
                                Pendiente
                            </td>


                            <td>
                                <a onclick="verpagocxc('5492','01');">
                                    <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos"
                                       aria-hidden="true"></i>
                                </a>





                                <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5492"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                    <i class="icmn-printer2 opc" aria-hidden="true"></i>
                                </a>
                                <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5492"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                    <i class="icmn-printer opc" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>

                        <tr                                                         class="tr_error"
                        >
                            <td><i data-toggle="tooltip" data-placement="right"
                                   data-original-title="5493" class="icmn-key2 opc"
                                   aria-hidden="true"></i></td>
                            <td>2021-04-10</td>
                            <td>001-002</td>
                            <td>000000838



                            </td>
                            <td>CONSUMIDOR FINAL</td>
                            <td>PRUEBAS</td>
                            <td>Demo</td>
                            <th class="estilovalor">
                                <div data-toggle="tooltip" data-html="true"
                                     title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                    9.500
                                </div>
                            </th>

                            <th class="estilovalor">0.000</th>
                            <th class="estilovalor">1.140</th>
                            <th class="estilovalor">10.64</th>
                            <td>

                                No Autorizado
                            </td>

                            <td>
                                Pendiente
                            </td>


                            <td>
                                <a onclick="verpagocxc('5493','01');">
                                    <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos"
                                       aria-hidden="true"></i>
                                </a>





                                <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5493"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                    <i class="icmn-printer2 opc" aria-hidden="true"></i>
                                </a>
                                <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5493"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                    <i class="icmn-printer opc" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>

                        <tr                                                         class="tr_error"
                        >
                            <td><i data-toggle="tooltip" data-placement="right"
                                   data-original-title="5494" class="icmn-key2 opc"
                                   aria-hidden="true"></i></td>
                            <td>2021-04-10</td>
                            <td>001-002</td>
                            <td>000000839



                            </td>
                            <td>ASESLIDER S.A.</td>
                            <td>PRUEBAS</td>
                            <td>Demo</td>
                            <th class="estilovalor">
                                <div data-toggle="tooltip" data-html="true"
                                     title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                    9.500
                                </div>
                            </th>

                            <th class="estilovalor">0.000</th>
                            <th class="estilovalor">1.140</th>
                            <th class="estilovalor">10.64</th>
                            <td>

                                No Autorizado
                            </td>

                            <td>
                                Pendiente
                            </td>


                            <td>
                                <a onclick="verpagocxc('5494','01');">
                                    <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos"
                                       aria-hidden="true"></i>
                                </a>





                                <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5494"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                    <i class="icmn-printer2 opc" aria-hidden="true"></i>
                                </a>
                                <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5494"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                    <i class="icmn-printer opc" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>

                        <tr                                                         class="tr_error"
                        >
                            <td><i data-toggle="tooltip" data-placement="right"
                                   data-original-title="5495" class="icmn-key2 opc"
                                   aria-hidden="true"></i></td>
                            <td>2021-04-10</td>
                            <td>001-002</td>
                            <td>000000840



                            </td>
                            <td>AGENCIA DE VIAJES ROSETUR...</td>
                            <td>PRUEBAS</td>
                            <td>Demo</td>
                            <th class="estilovalor">
                                <div data-toggle="tooltip" data-html="true"
                                     title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                    9.500
                                </div>
                            </th>

                            <th class="estilovalor">0.000</th>
                            <th class="estilovalor">1.140</th>
                            <th class="estilovalor">10.64</th>
                            <td>

                                No Autorizado
                            </td>

                            <td>
                                Pendiente
                            </td>


                            <td>
                                <a onclick="verpagocxc('5495','01');">
                                    <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos"
                                       aria-hidden="true"></i>
                                </a>





                                <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5495"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                    <i class="icmn-printer2 opc" aria-hidden="true"></i>
                                </a>
                                <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5495"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                    <i class="icmn-printer opc" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>

                        <tr                                                         class="tr_error"
                        >
                            <td><i data-toggle="tooltip" data-placement="right"
                                   data-original-title="5496" class="icmn-key2 opc"
                                   aria-hidden="true"></i></td>
                            <td>2021-04-10</td>
                            <td>001-002</td>
                            <td>000000841



                            </td>
                            <td>AGENCIA DE VIAJES ROSETUR...</td>
                            <td>PRUEBAS</td>
                            <td>Demo</td>
                            <th class="estilovalor">
                                <div data-toggle="tooltip" data-html="true"
                                     title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                    9.500
                                </div>
                            </th>

                            <th class="estilovalor">0.000</th>
                            <th class="estilovalor">1.140</th>
                            <th class="estilovalor">10.64</th>
                            <td>

                                No Autorizado
                            </td>

                            <td>
                                Pendiente
                            </td>


                            <td>
                                <a onclick="verpagocxc('5496','01');">
                                    <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos"
                                       aria-hidden="true"></i>
                                </a>





                                <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5496"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                    <i class="icmn-printer2 opc" aria-hidden="true"></i>
                                </a>
                                <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5496"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                    <i class="icmn-printer opc" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>

                        <tr                                                         class="tr_error"
                        >
                            <td><i data-toggle="tooltip" data-placement="right"
                                   data-original-title="5497" class="icmn-key2 opc"
                                   aria-hidden="true"></i></td>
                            <td>2021-04-10</td>
                            <td>001-002</td>
                            <td>000000842



                            </td>
                            <td>AGENCIA DE VIAJES ROSETUR...</td>
                            <td>PRUEBAS</td>
                            <td>Demo</td>
                            <th class="estilovalor">
                                <div data-toggle="tooltip" data-html="true"
                                     title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                    9.500
                                </div>
                            </th>

                            <th class="estilovalor">0.000</th>
                            <th class="estilovalor">1.140</th>
                            <th class="estilovalor">10.64</th>
                            <td>

                                No Autorizado
                            </td>

                            <td>
                                Pendiente
                            </td>


                            <td>
                                <a onclick="verpagocxc('5497','01');">
                                    <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos"
                                       aria-hidden="true"></i>
                                </a>





                                <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5497"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                    <i class="icmn-printer2 opc" aria-hidden="true"></i>
                                </a>
                                <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5497"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                    <i class="icmn-printer opc" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>

                        <tr                                                         class="tr_error"
                        >
                            <td><i data-toggle="tooltip" data-placement="right"
                                   data-original-title="5498" class="icmn-key2 opc"
                                   aria-hidden="true"></i></td>
                            <td>2021-04-10</td>
                            <td>001-002</td>
                            <td>000000843



                            </td>
                            <td>AGENCIA DE VIAJES ROSETUR...</td>
                            <td>PRUEBAS</td>
                            <td>Demo</td>
                            <th class="estilovalor">
                                <div data-toggle="tooltip" data-html="true"
                                     title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                    9.500
                                </div>
                            </th>

                            <th class="estilovalor">0.000</th>
                            <th class="estilovalor">1.140</th>
                            <th class="estilovalor">10.64</th>
                            <td>

                                No Autorizado
                            </td>

                            <td>
                                Pendiente
                            </td>


                            <td>
                                <a onclick="verpagocxc('5498','01');">
                                    <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos"
                                       aria-hidden="true"></i>
                                </a>





                                <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5498"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                    <i class="icmn-printer2 opc" aria-hidden="true"></i>
                                </a>
                                <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5498"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                    <i class="icmn-printer opc" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>

                        <tr                                                         class="tr_error"
                        >
                            <td><i data-toggle="tooltip" data-placement="right"
                                   data-original-title="5499" class="icmn-key2 opc"
                                   aria-hidden="true"></i></td>
                            <td>2021-04-10</td>
                            <td>001-002</td>
                            <td>000000844



                            </td>
                            <td>AGENCIA DE VIAJES ROSETUR...</td>
                            <td>PRUEBAS</td>
                            <td>Demo</td>
                            <th class="estilovalor">
                                <div data-toggle="tooltip" data-html="true"
                                     title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                    9.500
                                </div>
                            </th>

                            <th class="estilovalor">0.000</th>
                            <th class="estilovalor">1.140</th>
                            <th class="estilovalor">10.64</th>
                            <td>

                                No Autorizado
                            </td>

                            <td>
                                Pendiente
                            </td>


                            <td>
                                <a onclick="verpagocxc('5499','01');">
                                    <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos"
                                       aria-hidden="true"></i>
                                </a>





                                <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5499"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                    <i class="icmn-printer2 opc" aria-hidden="true"></i>
                                </a>
                                <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5499"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                    <i class="icmn-printer opc" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>

                        <tr                                                         class="tr_error"
                        >
                            <td><i data-toggle="tooltip" data-placement="right"
                                   data-original-title="5500" class="icmn-key2 opc"
                                   aria-hidden="true"></i></td>
                            <td>2021-04-10</td>
                            <td>001-002</td>
                            <td>000000845



                            </td>
                            <td>AGENCIA DE VIAJES ROSETUR...</td>
                            <td>PRUEBAS</td>
                            <td>Demo</td>
                            <th class="estilovalor">
                                <div data-toggle="tooltip" data-html="true"
                                     title="<b>Utilidad</b> 0.00<br> <b>Costo</b> 0.00">
                                    9.500
                                </div>
                            </th>

                            <th class="estilovalor">0.000</th>
                            <th class="estilovalor">1.140</th>
                            <th class="estilovalor">10.64</th>
                            <td>

                                No Autorizado
                            </td>

                            <td>
                                Pendiente
                            </td>


                            <td>
                                <a onclick="verpagocxc('5500','01');">
                                    <i class="fa fa-money opc" data-toggle="tooltip" data-original-title="Ver Pagos"
                                       aria-hidden="true"></i>
                                </a>





                                <a href="https://azur.com.ec/plataforma/factura/imprimir/normal/empresa/1/id/5500"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir">
                                    <i class="icmn-printer2 opc" aria-hidden="true"></i>
                                </a>
                                <a href="https://azur.com.ec/plataforma/factura/imprimir/pos/empresa/1/id/5500"
                                   data-toggle="tooltip" data-placement="top" data-original-title="Imprimir en POS">
                                    <i class="icmn-printer opc" aria-hidden="true"></i>
                                </a>
                            </td>
                        </tr>

                    </table>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <!-- Previous Page Link -->
                        <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1">&laquo;</a>
                        </li>

                        <!-- Pagination Elements -->
                        <!-- "Three Dots" Separator -->

                        <!-- Array Of Links -->
                        <li class="page-item active">
                            <a class="page-link" href="#">
                                <span class="">1</span>
                            </a>
                        </li>
                        <li><a class="page-link" href="https://azur.com.ec/plataforma/factura/vertodos?page=2">2</a></li>

                        <!-- Next Page Link -->
                        <li><a class="page-link" href="https://azur.com.ec/plataforma/factura/vertodos?page=2" rel="next">&raquo;</a></li>
                    </ul>
                </nav>
            </div>
            <!-- box-footer -->
        </div>
        <!-- /.box -->

        <div class="modal fade in" id="modal-pagos">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">
                            Pagos
                            <span id="id_texto_pago"></span>

                            <button onclick="nuevopago()" type="button" data-toggle="tooltip"
                                    data-original-title="Nuevo Pago"
                                    aria-hidden="true" class="btn btn-primary ">
                                <i class="fa fa-money "></i> &nbsp;Nuevo Pago
                            </button>

                            <button onclick="nuevaretencionrecibida()" type="button" data-toggle="tooltip"
                                    data-original-title="Nueva Retención Recibida"
                                    aria-hidden="true" class="btn btn-primary ">
                                <i class="fa fa-money "></i> &nbsp;Nueva Retención Recibida
                            </button>
                        </h4>

                    </div>
                    <div class="modal-body " id="animacionpagos">

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover ">
                                <thead class="thead-dark">
                                <td>Id</td>
                                <td>Fecha</td>
                                <td>Fecha Vencimiento</td>
                                <td>Concepto</td>
                                <td>Débito</td>
                                <td>Crédito</td>
                                <td>X</td>
                                </thead>
                                <tbody id="id_tabla_pagos">

                                </tbody>
                                <tfoot id="id_tabla_pagosfooter">

                                </tfoot>
                            </table>
                        </div>

                        <button type="button" class="btn btn-danger " data-dismiss="modal">Cerrar</button>
                    </div>


                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div class="modal fade in" id="modal-nuevopago">
            <div class="modal-dialog modal-xs">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">
                            Nuevo Pago
                        </h4>

                    </div>
                    <div class="modal-body " id="animacionpagos">
                        <form id="formulariopago" enctype="multipart/form-data">
                            <input type="hidden" id="pago_id_comprobante">
                            <input type="hidden" id="pago_tipo_comprobante">

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        Fecha
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="pago_fecha" value="2021-04-19">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        Concepto
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" id="pago_concepto">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        Imagen
                                        <small>(Opcional)</small>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="file" id="pago_archivo" name="pago_archivo" accept="image/*"
                                               class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        Forma de Pago
                                        <small>(Opcional)</small>
                                    </div>
                                    <div class="col-md-9">
                                        <select id="pago_formapago" class="form-control" name="pago_formapago">
                                            <option value="01">Sin utilizacion del sistema financiero</option>
                                            <option value="16">Tarjetas de Debito</option>
                                            <option value="17">Dinero Electronico</option>
                                            <option value="18">Tarjeta Prepago</option>
                                            <option value="19">Tarjeta de Credito</option>
                                            <option selected="" value="20">Otros con Utilizacion del Sistema Financiero</option>
                                            <option value="21">Endoso de Titulos</option>
                                            <option value="15">COMPENSACIÓN DE DEUDAS</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        Valor
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" id="pago_valor" class="form-control validador_numero2 usd2">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        Observacion
                                        <small>(Opcional)</small>
                                    </div>
                                    <div class="col-md-9">
                                        <textarea type="text" class="form-control" id="pago_observacion"></textarea>
                                    </div>
                                </div>
                            </div>
                        </form>


                        <button type="button" class="btn btn-danger " data-dismiss="modal">Cerrar</button>
                        <button onclick="guardarpagocxc()" type="button" data-toggle="tooltip"
                                data-original-title="Guardar Pago"
                                aria-hidden="true" class="btn btn-primary pull-right">
                            <i class="fa fa-money "></i> &nbsp;Guardar
                        </button>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->


        <div class="modal fade in" id="modal-nuevoretencionrecibida">
            <div class="modal-dialog modal-xs">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">
                            Nueva Retención Recibida
                        </h4>

                    </div>
                    <div class="modal-body " id="animacionretencionrecibida">
                        <form id="formularioretencionrecibida" enctype="multipart/form-data">
                            <input type="hidden" id="retencion_id_comprobante">
                            <input type="hidden" id="retencion_tipo_comprobante">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        Fecha
                                    </div>

                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="retencion_fecha" value="2021-04-19">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        Tipo Impuesto
                                    </div>
                                    <div class="col-md-9">
                                        <select name="retencion_tipo" id="retencion_tipo" class="form-control width-middle">
                                            <option selected="true" value="2">IVA</option>
                                            <option value="1">RENTA</option>
                                            <option value="6">ISD</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="animacionrenta">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            Codigo Retencion
                                        </div>
                                        <div class="col-md-9">
                                            <select id="codigoretencion" name="codigoretencion" class="form-control">


                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="animacioncodigo">
                                    <div class="row">
                                        <div class="col-md-9">
                                            Porcentaje
                                        </div>
                                        <div class="col-md-3">
                                            <div id="porcentajediv">
                                                <div class="input-group">
                                                    <input type="number" class="form-control usd5 " id="porcentaje"
                                                           name="porcentaje"
                                                           value="0">
                                                    <div class="input-group-addon">%</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                Base Imponible
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" id="retencion_base"
                                                       class="form-control validador_numero2 usd2">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3">
                                                Valor
                                                <button type="button" class="btn-primary btn-sm"
                                                        onclick="calcularvaloretenido()"><i class="fa fa-fw fa-refresh"></i>
                                                </button>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" id="retencion_valor"
                                                       class="form-control validador_numero2 usd2">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        Nro Comprobante Retención
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" id="retencion_secuencial">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3">
                                        Observacion
                                        <small>(Opcional)</small>
                                    </div>
                                    <div class="col-md-9">
                                        <textarea type="text" class="form-control" id="retencion_observacion"></textarea>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <button type="button" class="btn btn-danger " data-dismiss="modal">Cerrar</button>
                        <button onclick="guardarretencionrecibidacxc()" type="button" data-toggle="tooltip"
                                data-original-title="Guardar Retención Recibida"
                                aria-hidden="true" class="btn btn-primary pull-right">
                            <i class="fa fa-money "></i> &nbsp;Guardar
                        </button>
                    </div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>


        <div class="modal fade in" id="modal-fotopago">
            <div class="modal-dialog modal-xs">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">
                            Foto
                        </h4>

                    </div>
                    <div class="modal-body ">
                        <img id='cargafoto' src="" style="width: 100%; height: 100%" alt="">
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-dialog -->

        <script>


            var retencionbaseiva = "";
            var retencionbaserenta = "";

            $('#retencion_tipo').on('change', function () {

                if (this.value == 2) {
                    $("#retencion_base").val(retencionbaseiva);
                } else if (this.value == 1) {
                    $("#retencion_base").val(retencionbaserenta);
                } else {
                    $("#retencion_base").val("");
                }

            });


            function guardarretencionrecibidacxc() {
                var id_comprobante = $("#pago_id_comprobante").val();
                obtenercodigo();
                swal({
                        title: "Esta Seguro que desea Retención Recibida",
                        text: "Guardar Retención Recibida",
                        type: "info",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true,
                    },
                    function () {
                        $.ajax({
                            type: 'get',
                            url: "https://azur.com.ec/plataforma/guardarretencionrecibidacxc",
                            data: {
                                id_comprobante: $("#pago_id_comprobante").val(),
                                tipo_comprobante: $("#pago_tipo_comprobante").val(),
                                retencion_fecha: $("#retencion_fecha").val(),
                                retencion_tipo: $("#retencion_tipo").val(),
                                retencion_base: $("#retencion_base").val(),
                                retencion_valor: $("#retencion_valor").val(),
                                retencion_observacion: $("#retencion_observacion").val(),
                                retencion_secuencial: $("#retencion_secuencial").val(),
                                retencion_porcentaje: $("#porcentaje").val(),
                                retencion_codigoretencion: $("#codigoretencion").attr("codigo"),
                                api_key2: "API_1_2_5a4492f2d5137"
                            },
                            success: function (resp) {

                                if (resp.respuesta == true) {
                                    swal({
                                        title: "Excelente!",
                                        text: "Guardado",
                                        type: "success",
                                        showConfirmButton: false,
                                        timer: 1000
                                    });
                                    $('#modal-nuevoretencionrecibida').modal('hide');
                                    verpagocxc(resp.id_comprobante, resp.tipo_comprobante);

                                } else {
                                    swal({
                                        title: "ERROR!",
                                        text: "Error en el proceso Vuelva a intentarlo",
                                        type: "error",
                                        showConfirmButton: false,
                                        timer: 1000
                                    });
                                }
                            }, error: function (xhr) {
                                erroresenajax(xhr);
                            },
                        });
                    });

            }


            $(function () {
                $('#retencion_secuencial').mask("000-000-000000000", {placeholder: "___-___-_________"});
            })

            function nuevaretencionrecibida() {
                $('#modal-nuevoretencionrecibida').modal('show');
                $("#retencion_base").val(retencionbaseiva);
                $("#retencion_valor").val("");
                $("#retencion_observacion").val("");
                codigosretencion();
            }

            function guardarpagocxc() {
                var id_comprobante = $("#pago_id_comprobante").val();
                var paqueteDeDatos = new FormData();
                paqueteDeDatos.append('foto', $('#pago_archivo')[0].files[0]);
                paqueteDeDatos.append('pago_archivo', $('#pago_archivo')[0].files[0]);
                paqueteDeDatos.append('id_comprobante', $("#pago_id_comprobante").val());
                paqueteDeDatos.append('tipo_comprobante', $("#pago_tipo_comprobante").val());
                paqueteDeDatos.append('pago_fecha', $("#pago_fecha").val());
                paqueteDeDatos.append('pago_concepto', $("#pago_concepto").val());
                paqueteDeDatos.append('pago_formapago', $("#pago_formapago").val());
                paqueteDeDatos.append('pago_valor', $("#pago_valor").val());
                paqueteDeDatos.append('pago_observacion', $("#pago_observacion").val());
                paqueteDeDatos.append('api_key2', "API_1_2_5a4492f2d5137");

                swal({
                        title: "Esta Seguro que desea Guardar el Pago",
                        text: "Guardar Pago",
                        type: "info",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true,
                    },
                    function () {

                        $.ajax({
                            type: 'POST',
                            url: "https://azur.com.ec/plataforma/guardarpagosdecxc",
                            contentType: false,
                            processData: false,
                            cache: false,
                            data: paqueteDeDatos,
                            success: function (resp) {

                                if (resp.respuesta == true) {
                                    swal({
                                        title: "Excelente!",
                                        text: "Guardado",
                                        type: "success",
                                        showConfirmButton: false,
                                        timer: 1000
                                    });
                                    $('#modal-nuevopago').modal('hide');
                                    verpagocxc(resp.id_comprobante, resp.tipo_comprobante);

                                } else {
                                    swal({
                                        title: "ERROR!",
                                        text: "Error en el proceso Vuelva a intentarlo",
                                        type: "error",
                                        showConfirmButton: false,
                                        timer: 1000
                                    });
                                }
                            }, error: function (xhr) {
                                erroresenajax(xhr);
                            },
                        });
                    });


            }

            function nuevopago() {
                $('#modal-nuevopago').modal('show');
            }

            function verpagocxc(id_comprobante, tipo_comprobante) {

                retencionbaseiva = 0;
                retencionbaserenta = 0;
                $('#id_tabla_pagos').empty();
                $('#pago_id_comprobante').val("");
                $('#pago_tipo_comprobante').val("");
                $('#retencion_id_comprobante').val("");
                $('#retencion_tipo_comprobante').val("");
                $('#modal-pagos').modal('show');
                $("#retencion_valor").val("");
                $("#retencion_observacion").val("");
                $("#retencion_secuencial").val("");
                $("#animacionpagos").LoadingOverlay("show");
                $('#pago_id_comprobante').val(id_comprobante);
                $('#retencion_id_comprobante').val(id_comprobante);
                $('#pago_tipo_comprobante').val(tipo_comprobante);
                $('#retencion_tipo_comprobante').val(tipo_comprobante);

                var auxtexto = "";
                if (tipo_comprobante == "01" || tipo_comprobante == "1") {
                    auxtexto = "Factura";
                } else if (tipo_comprobante == "02" || tipo_comprobante == "2") {
                    auxtexto = "Nota de Venta";
                } else if (tipo_comprobante == "05" || tipo_comprobante == "5") {
                    auxtexto = "Nota de Debito";
                } else {
                    auxtexto = "otro";
                }

                $.ajax({
                    type: 'POST',
                    url: "https://azur.com.ec/plataforma/pagosdecomprobantecxc",
                    data: {
                        id_comprobante: id_comprobante,
                        tipo_comprobante: tipo_comprobante,
                        api_key2: "API_1_2_5a4492f2d5137"
                    },
                    success: function (resp) {
                        if (resp.respuesta == true) {
                            $('#id_texto_pago').html(auxtexto + " # " + resp.numero);
                            $('#id_tabla_pagos').empty();
                            $('#id_tabla_pagosfooter').empty();
                            $("#pago_concepto").val("");
                            $("#pago_valor").val("");
                            $(resp.transacciones).each(function (i, v) {
                                var debito = 0;
                                var credito = 0;
                                if (v.tipo == 2) {
                                    credito = v.valor;
                                    debito = 0;
                                } else if (v.tipo == 1) {
                                    credito = 0;
                                    debito = v.valor;
                                }

                                var botoneliminar = "";

                                if (v.tipo == 2) {
                                    botoneliminar = '<a id="eliminarpagocxc" id_pago="' + v.id + '"><i class="fa fa-times-circle anadir"></i></a>';
                                } else if (v.tipo == 1) {
                                    botoneliminar = '';
                                }

                                var fotopago = '';

                                if (v.foto == "" || v.foto == null) {
                                    var fotopago = '';
                                } else if (v.foto != "") {
                                    var fotopago = '<button onclick="verfotopago(' + v.id + ')" type="button" data-toggle="tooltip" data-original-title="Ver Pago" aria-hidden="true" class="btn btn-primary ">  <i class="fa fa-image "></i> </button>';
                                }

                                if (v.observacion == "" || v.observacion == null) {
                                    var observacion = '';
                                } else if (v.observacion != "") {
                                    var observacion =  v.observacion;
                                }


                                $("#id_tabla_pagos").append('<tr>' +
                                    '<td>' + v.id + fotopago + '</td>' +
                                    '<td>' + v.fecha + '</td>' +
                                    '<td>' + v.vencimiento + '</td>' +
                                    '<td>' + v.concepto +'<br>'+observacion+ '</td>' +
                                    '<td style="text-align: right">' + debito + '</td>' +
                                    '<td style="text-align: right">' + credito + '</td>' +
                                    '<td>' + botoneliminar + '</td>' +
                                    '</tr>');
                            })
                            $("#id_tabla_pagosfooter").append('<tr>' +
                                '<td colspan="4"></td>' +
                                '<td><b>Saldo</b></td>' +
                                '<td style="text-align: right">' + resp.saldo + '</td>' +
                                '</tr>');

                            $("#pago_concepto").val("Pago " + auxtexto + " # " + resp.numero);
                            $("#pago_valor").val(resp.saldo);
                            retencionbaseiva = resp.baseiva;
                            retencionbaserenta = resp.baserenta;
                            $("#animacionpagos").LoadingOverlay("hide");

                        } else {
                            $("#animacionpagos").LoadingOverlay("hide");

                        }
                    }, error: function (xhr) {
                        erroresenajax(xhr);
                    },
                })
            }


            function eliminarpagocxc(id_pago) {
                $.ajax({
                    type: 'POST',
                    url: "https://azur.com.ec/plataforma/eliminarpagosdecxc",
                    data: {
                        id_pago: id_pago,
                        api_key2: "API_1_2_5a4492f2d5137"
                    },
                    success: function (resp) {
                        if (resp.respuesta == true) {
                            if (resp.id_comprobante != "") {
                                verpagocxc(resp.id_comprobante, resp.tipo_comprobante)
                            }
                            swal({
                                title: "Excelente!",
                                text: "Pago Eliminado",
                                type: "success",
                                showConfirmButton: false,
                                timer: 1000
                            });

                        } else {
                            swal({
                                title: "ERROR!",
                                text: "Error en el proceso Vuelva a intentarlo",
                                type: "error",
                                showConfirmButton: false,
                                timer: 1000
                            });
                        }
                    }, error: function (xhr) {
                        erroresenajax(xhr);
                    },
                })
            }

            $(function () {
                $('#pago_fecha').datetimepicker({

                    widgetPositioning: {
                        horizontal: 'left'
                    },
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    },
                    format: 'YYYY-MM-DD'
                });
            })

            $('#id_tabla_pagos').on('click', '#eliminarpagocxc', function () {
                var id_pago = this.getAttribute("id_pago");
                var fila = $(this).parents('tr');
                fila.remove();
                eliminarpagocxc(id_pago);
            })


            function verfotopago(id_pago) {
                $.ajax({
                    type: 'POST',
                    url: "https://azur.com.ec/plataforma/verpagofotocxc",
                    data: {
                        id_pago: id_pago,
                        api_key2: "API_1_2_5a4492f2d5137"
                    },
                    success: function (resp) {
                        if (resp.respuesta == true) {
                            $('#cargafoto').attr('src', 'data:image/jpeg;base64,' + resp.foto);
                            $('#modal-fotopago').modal('show');
                        } else {
                        }
                    }
                })
            }


            $('#retencion_tipo').change(function () {

                codigosretencion();
            });

            function codigosretencion() {
                $(".animacionrenta").LoadingOverlay("show");
                var codigoimpuesto = $('#retencion_tipo').val();
                $.ajax({
                    type: 'POST',
                    url: "https://azur.com.ec/plataforma/impuestos",
                    data: {
                        codigo: codigoimpuesto,
                        api_key2: "API_1_2_5a4492f2d5137",
                    },
                    success: function (resp) {
                        if (resp.respuesta == true) {
                            $('#codigoretencion').empty();
                            $(resp.datos).each(function (i, v) {
                                $('#codigoretencion').append(' <option value=' + v.id + '>' + v.codigoretencion + " - " + v.descripcion.substr(0, 100) + '</option>');
                            });
                            obtenercodigo();
                            porcentajeretencion();
                            $(".animacionrenta").LoadingOverlay("hide");
                        } else {
                            $(".animacionrenta").LoadingOverlay("hide");
                        }
                    }
                })
            }

            function porcentajeretencion() {
                $(".animacioncodigo").LoadingOverlay("show");
                var codigo = $('#codigoretencion').val();

                $.ajax({
                    type: 'POST',
                    url: "https://azur.com.ec/plataforma/impuestosporcentaje",
                    data: {
                        codigo: codigo,
                        api_key2: "API_1_2_5a4492f2d5137",
                    },
                    success: function (resp) {
                        $('#porcentaje').val("");
                        if (resp.respuesta == true) {
                            $('#porcentaje').val(resp.valor);
                            calcularvaloretenido();
                            $(".animacioncodigo").LoadingOverlay("hide");
                        } else {
                            $(".animacioncodigo").LoadingOverlay("hide");
                        }
                    }
                })
            }

            $('#codigoretencion').change(function () {
                porcentajeretencion();
            });

            function calcularvaloretenido() {
                var baseimponible = $('#retencion_base').val();
                var porcentaje = $('#porcentaje').val();
                var total = redondear2(parseFloat(baseimponible) * (parseFloat(porcentaje) / 100));
                $("#retencion_valor").val(total);
            }


            $('#codigoretencion').change(function () {
                obtenercodigo();
            });

            function obtenercodigo() {
                $(".animacionrenta").LoadingOverlay("show");
                var id = $('#codigoretencion').val();
                ;
                $.ajax({
                    type: 'POST',
                    url: "https://azur.com.ec/plataforma/impuestoscodigo",
                    data: {
                        id: id,
                        api_key2: "API_1_2_5a4492f2d5137",
                    },
                    success: function (resp) {
                        if (resp.respuesta == true) {

                            $('#codigoretencion').attr("codigo", resp.valor);
                            $(".animacionrenta").LoadingOverlay("hide");
                        } else {
                            $(".animacionrenta").LoadingOverlay("hide");
                        }
                    }
                })
            }
        </script>

        <script>



            $(function () {

                $('#fechainicio').datetimepicker({
                    widgetPositioning: {
                        horizontal: 'left'
                    },
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    },
                    format: 'YYYY-MM-DD'
                });

                $('#fechafin').datetimepicker({

                    widgetPositioning: {
                        horizontal: 'left'
                    },
                    icons: {
                        time: "fa fa-clock-o",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down"
                    },
                    format: 'YYYY-MM-DD'
                });

            })

        </script>
    </section>
    <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 4.0
        </div>
        <strong>Copyright &copy; 2020 Azur Facturación Electronica  .</strong> All rights
        reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
            <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <!-- Home tab content -->
            <div class="tab-pane active" id="control-sidebar-home-tab">
                usuario 1<br>
                vendedor 3 <br>
                empresa 1<br>
                establecimiento 1<br>
                punto emision 16<br>
                tipo usuario vendedor 1<br>
                ambiente 1<br>
                plan Express<br>
                demo 1 <br>
                api_key API_1_2_5a4492f2d5137 <br>


            </div>
            <!-- /.tab-pane -->

            <!-- Settings tab content -->
            <div class="tab-pane" id="control-sidebar-settings-tab">
                Permisos Plan


                cantidadcomprobantes= 450<br>
                facturacion= 1<br>
                retenciones= 1<br>
                notascredito= 1<br>
                notasdebito= 1<br>
                guias= 1<br>
                recibos= 0<br>
                proformas= 1<br>
                cantidadvendedores= 3<br>
                cantidadempresas= 1<br>
                cantidadestablecimientos= 3<br>
                cantidadpuntosemision= 3<br>
                buzoncomprobantes= 1<br>
                cuentasporcobrar= 1<br>
                reportes= 1<br>
                smtppropio= 1<br>
                precio= 70<br>
                notaventa= 1<br>
                liquidacion= 1<br>
                <br>
                Permisos Vendedor
                <br>

                pro_crear= 1<br>
                rec_crear= 1<br>
                deb_crear= 1<br>
                guia_crear= 1<br>
                ret_crear= 1<br>
                cre_cambiarprecio= 1<br>
                cre_crear= 1<br>
                fac_cambiarprecio= 1<br>
                fac_crear= 1<br>
                produc_crear= 1<br>
                produc_editar= 1<br>
                report_principal= 1<br>
                report_total= 1<br>
                produc_inventario= 1<br>
                produc_eliminar= 1<br>
                recibidos_ver= 1<br>
                compras_ver= 1<br>

            </div>
            <!-- /.tab-pane -->
        </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>

    </div>
    <!-- ./wrapper -->


    <!-- AdminLTE App -->
    <script src="https://azur.com.ec/dist/js/adminlte.min.js"></script>
    <!-- Sparkline -->
    <script src="https://azur.com.ec/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- jvectormap  -->
    <script src="https://azur.com.ec/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="https://azur.com.ec/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- SlimScroll -->
    <script src="https://azur.com.ec/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>


    <script src="https://azur.com.ec/assets/vendors/bootstrap-sweetalert/dist/sweetalert.min.js"></script>


    <!-- AdminLTE for demo purposes
    <script src="https://azur.com.ec/dist/js/demo.js"></script>

    -->

    <script src="https://azur.com.ec/js/loadingoverlay.min.js"></script>
    <script src="https://azur.com.ec/js/imprimirarea.js"></script>
    <script>


        function actualizarambiente() {
            $("#animacionambiente").LoadingOverlay("show");
            var id_establecimiento = $("#id_establecimiento").val();
            var id_empresa = $("#id_empresa").val();
            var id_puntoemision = $("#id_puntoemision").val();
            $.ajax({
                type: 'get',
                url: "https://azur.com.ec/plataforma/cuenta/listadeambientes",
                data: {
                    id_establecimiento: id_establecimiento,
                    id_empresa: id_empresa,
                    id_puntoemision: id_puntoemision,
                    api_key2: "API_1_2_5a4492f2d5137"
                },
                success: function (resp) {

                    if (resp.respuesta == true) {
                        $("#id_ambiente").val(resp.dato);
                    }
                    $("#animacionambiente").LoadingOverlay("hide");
                }
            });
        }

        function actualizarpuntosdeemision() {
            $("#animacionpuntosdeemision").LoadingOverlay("show");
            var id_establecimiento = $("#id_establecimiento").val();
            var id_empresa = $("#id_empresa").val();
            console.log(id_establecimiento);
            console.log(id_empresa);
            $.ajax({
                type: 'get',
                url: "https://azur.com.ec/plataforma/cuenta/listapuntosdeemision",
                data: {
                    id_establecimiento: id_establecimiento,
                    id_empresa: id_empresa,
                    api_key2: "API_1_2_5a4492f2d5137"
                },
                success: function (resp) {

                    if (resp.respuesta == true) {
                        $('#id_puntoemision').empty();
                        $(resp.datos).each(function (i, v) {
                            $('#id_puntoemision').append(' <option value=' + v.id + '>' + v.nombre + '</option>');
                        });
                        $("#animacionpuntosdeemision").LoadingOverlay("hide");
                        actualizarambiente();
                    }
                }
            });
        }

        function actualizarestablecimientos() {
            $("#animacionestablecimiento").LoadingOverlay("show");
            $.ajax({
                type: 'get',
                url: "https://azur.com.ec/plataforma/cuenta/listadeestablecimientos",
                data: {
                    api_key2: "API_1_2_5a4492f2d5137",
                    id_empresa: $("#id_empresa").val(),
                },
                success: function (resp) {

                    if (resp.respuesta == true) {
                        $('#id_establecimiento').empty();
                        $(resp.datos).each(function (i, v) {
                            $('#id_establecimiento').append(' <option value=' + v.id + '>' + v.nombre + '</option>');
                        });
                        $("#animacionestablecimiento").LoadingOverlay("hide");
                        actualizarpuntosdeemision();
                    }
                }
            });
        }

        $("#id_empresa").change(function () {
            actualizarestablecimientos();
        });
        $("#id_establecimiento").change(function () {
            actualizarpuntosdeemision();
        });
        $("#id_puntoemision").change(function () {
            actualizarambiente();
        })

    </script>
    <script>
        $('.user-menu a').on('click', function (event) {
            $(this).parent().toggleClass('open');
        });
    </script>





    </body>
    </html>




@endsection
@section('scripts')
    @include('invoices.partials._invoices_js')
@endsection

