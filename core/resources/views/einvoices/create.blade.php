@extends('app')
@section('content')

    <section class="content-header">

        <h1>
            Nueva Factura en AZUR FACTURACIÓN ELECTRÓNICA Codigo Estab. 001

        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Azur Facturación Electronica </a>
            </li>
            <li class="active">Factura</li>
        </ol>
    </section>

    <section class="content">
        <div class="box">
            <div class="box-body">
                <div class="row">

                    <div class="col-md-2">
                        <b>Fecha</b>
                        <input type="text" class="form-control" id="fecha" value="2021-04-18">
                    </div>
                    <div class="col-md-2">
                        <b>Tipo</b>
                        <select class="form-control" name="tipo_tecnologia" disabled="" id="tipo_tecnologia"><option selected="true" value="1">Electrónica</option></select>
                    </div>
                    <div class="col-md-2">
                        <b>Punto Emisión</b>
                        <select class="form-control" disabled="true" id="punto_emision" name="punto_emision"><option value="1">003-Otros</option><option selected="true" value="16">002-Caja 2</option><option value="35">001-General</option><option value="1505">900-Caja 2</option><option value="2752">901-901</option></select>
                    </div>
                    <div class="col-md-2">
                        <b>Secuencial</b>
                        <input class="form-control validador_numeroentero" type="number" value="" id="secuencial">
                    </div>

                    <div class="col-md-2">
                        <b>Ambiente</b>
                        <select class="form-control" name="ambiente" id="ambiente"><option selected="true" value="1">Pruebas</option></select>
                    </div>
                </div>


            </div>
        </div>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Datos del Comprador</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col col-md-3">
                        Cliente
                        <div class="input-group">
                            <input id="criteriocliente" name="criteriocliente" class="form-control" type="text" value="" placeholder="Ingrese su Busqueda">
                            <span class="input-group-btn">
                            <button id="botonbuscarextraboton" type="button" onclick="prueba()" class="btn btn-primary btn-flat"><i class="fa  fa-search"></i></button>
                          </span>
                        </div>




                    </div>
                    <div class="col col-md-3">
                        Seleccione un Resultado
                        <select class="form-control" id="clientes">
                        </select>
                    </div>

                    <div class="col col-md-3">
                        Sucursal
                        <select class="form-control" id="sucursalesclientes">
                        </select>
                    </div>
                    <div class="col col-md-3">
                        <div class="btn-group">
                            <button type="button" class="btn btn-success btn-sm" id="botonnuevocliente"> <i class="fa fa-plus-square"></i> &nbsp;Nuevo</button>
                            <button type="button" disabled="disabled" class="btn btn-warning btn-sm" id="botoneditarcliente"> <i class="fa fa-edit"></i>&nbsp;Editar</button>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row" id="contenedorcliente">
                <div class="col col-md-12">
                    <div class="col-md-12  ">
                        <div class="form-group row">
                            <input type="hidden" id="tipo_evento_cliente" value="final">
                            <input type="hidden" id="id_cliente" value="">
                            <input type="hidden" id="id_sucursal" value="">
                            <div class="col-md-2">
                                <label class="form-control-label" for="l0">Tipo Identificacion</label>
                            </div>
                            <div class="col-md-4">
                                <select class="form-control requerido" name="tipoidentificacion" id="tipoidentificacion" disabled="disabled">
                                    <option selected="" value="05">Cedula</option>
                                    <option value="04">Ruc</option>
                                    <option value="06">Pasaporte</option>
                                    <option value="08">Identificacion del Exterior</option>
                                    <option value="09">Placa</option>
                                </select>
                            </div>

                            <div class="col-md-2">
                                <label class="form-control-label " for="l0">Identificacion</label>
                            </div>
                            <div class="col-md-4">
                                <input name="identificacion" type="text" class="form-control requerido " placeholder="9999999999999" id="identificacion" disabled="disabled">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="l0">Nombres/Razon Social</label>
                            </div>
                            <div class="col-md-4">
                                <input name="nombrerazonsocial" type="text" class="form-control requerido mayuscula" placeholder="Consumidor Final" id="nombrerazonsocial" disabled="disabled">
                            </div>
                            <div class="col-md-2">
                                <label class="form-control-label" for="l0">Dirección</label>
                            </div>
                            <div class="col-md-4">
                                <input name="direccion" type="text" class="form-control requerido mayuscula" placeholder="N/D" id="direccion" disabled="disabled">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group row">


                            <div class="col-md-1">
                                <label class="form-control-label" for="l0">Telefono</label>
                            </div>
                            <div class="col-md-2">
                                <input name="telefono" type="text" class="form-control requerido" placeholder="N/D" id="telefono" disabled="disabled">
                            </div>
                            <div class="col-md-1">
                                <label class="form-control-label" for="l0">Celular</label>
                            </div>
                            <div class="col-md-2">
                                <input name="celular" type="text" class="form-control" placeholder="N/D" id="celular" disabled="disabled">
                            </div>
                            <div class="col-md-2">
                                <label class="form-control-label" for="l0">Correo</label>
                            </div>
                            <div class="col-md-4">
                                <input name="correo" type="text" class="form-control requerido" placeholder="N/D" id="correo" disabled="disabled">
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12  ">
                        <div class="form-group row">
                            <div class="col-md-1">
                                <label class="form-control-label">Sucursal</label>
                            </div>
                            <div class="col-md-2  ">
                                <input name="sucursal" type="text" class="form-control mayuscula" id="sucursal" disabled="disabled">
                            </div>
                            <div class="col-md-2">
                                <label class="form-control-label">Codigo Sucursal</label>
                            </div>
                            <div class="col-md-1  ">
                                <input name="sucursal" type="text" class="form-control mayuscula" id="codigosucursal" disabled="disabled">
                            </div>
                            <div class="col-md-1">
                                <label class="form-control-label" for="l0">Provincia</label>
                            </div>
                            <div class="col-md-2">

                                <select class="form-control" id="provincia" name="provincia" disabled="disabled">
                                    <option value="1064">Azuay</option>
                                    <option value="1061">Bolivar</option>
                                    <option value="1063">Cañar</option>
                                    <option value="1056">Carchi</option>
                                    <option value="1062">Chimborazo</option>
                                    <option value="1059">Cotopaxi</option>
                                    <option value="1055">El Oro</option>
                                    <option value="1053">Esmeraldas</option>
                                    <option value="888">Exterior</option>
                                    <option value="1072">Galapagos</option>
                                    <option value="1050">Guayas</option>
                                    <option value="1057">Imbabura</option>
                                    <option value="1065">Loja</option>
                                    <option value="1051">Los Rios</option>
                                    <option value="1052">Manabi</option>
                                    <option value="1070">Morona Santiago</option>
                                    <option value="1067">Napo</option>
                                    <option value="1068">Orellana</option>
                                    <option value="1069">Pastaza</option>
                                    <option value="1058">Pichincha</option>
                                    <option value="1054">Santa Elena</option>
                                    <option value="1074">Santo Domingo</option>
                                    <option selected="true" value="999">Sin Especificar</option>
                                    <option value="1066">Sucumbios</option>
                                    <option value="1060">Tungurahua</option>
                                    <option value="1071">Zamora Chinchipe</option>
                                </select>

                            </div>
                            <div class="col-md-1">
                                <label class="form-control-label" for="l0">Ciudad</label>
                            </div>
                            <div class="col-md-2">
                                <select name="ciudad" class="form-control" id="ciudad" disabled="disabled">
                                    <option selected="true" value="999">Sin Especificar</option>
                                </select>

                            </div>
                        </div>
                    </div>


                </div>
                <div class="col-md-12">
                    &nbsp; *Si desea Crear Otro Sucursal Primero Crearlo en Menú-&gt;Clientes-&gt;Editar Sucursales.
                </div>
            </div>
        </div>








        <div class="box">
            <div class="box-header with-border">

                <h3 class="box-title">Productos y Servicios</h3>
                <button type="button" class="btn  btn-primary btn-sm flat" data-toggle="modal" data-target="#modal-productos">
                    <i class="fa fa-search"></i> Buscar / Añadir
                </button>
                <div class="modal fade" id="modal-productos" style="display: none;">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                <h4 class="modal-title">Productos y Servicios</h4>

                            </div>
                            <div class="modal-body">
                                <div class="col col-md-12">
                                    <div class="row efectobus">

                                        <div class="col col-md-3">

                                            Producto
                                            <div class="input-group">
                                                <input id="criterioproducto" class="form-control" type="text" value="" placeholder="Ingrese su Busqueda">
                                                <span class="input-group-btn">
                            <button id="botonbuscarextrabotonproducto" type="button" class="btn btn-primary btn-flat"><i class="fa  fa-search"></i></button>
                          </span>
                                            </div>


                                        </div>
                                        <div class="col col-md-5">
                                            Seleccione un Resultado
                                            <select class="form-control   selectator" id="productos" style="display: none;">
                                            </select><div id="selectator_productos" class="selectator_element single options-hidden" style="width: 100%; min-height: 0px; padding: 4px 10px; flex-grow: 0; position: relative;"><span class="selectator_textlength" style="position: absolute; visibility: hidden;"></span><div class="selectator_selected_items"></div><input class="selectator_input" placeholder="Search..." autocomplete="false"><ul class="selectator_options"></ul></div>
                                        </div>

                                        <div class="col col-md-2">
                                            Bodega
                                            <select class="form-control" name="bodegas" id="bodegas"><option value="1">Principal</option><option value="27">Prueba bodega</option><option value="28">oTRA BODEGA</option><option value="29">Keyla</option><option value="30">Nadia Perez</option><option value="31">Bodega Manta</option></select>
                                        </div>
                                        <div class="col col-md-2">
                                            <div class="btn-group">
                                                <button type="button" style="white-space: normal!important;" class="btn btn-success" onclick="" id="botonnuevoproducto">Nuevo Producto/Servicio
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12  ">
                                    <div class="form-group row">
                                        <div class="col-md-1">
                                            <label class="form-control-label" for="l0">Codigo</label>
                                        </div>
                                        <div class="col-md-2">
                                            <input name="producto_id" type="hidden" class="form-control " placeholder="" id="producto_id">
                                            <input name="producto_unidad" type="hidden" class="form-control " placeholder="" id="producto_unidad">
                                            <input name="producto_codigoauxiliar" type="hidden" class="form-control " placeholder="" id="producto_codigoauxiliar">
                                            <input name="producto_codigo" type="text" class="form-control requerido" placeholder="" id="producto_codigo" disabled="disabled">
                                            <input name="producto_icedefecto" type="hidden" class="form-control " placeholder="" id="producto_icedefecto">
                                        </div>
                                        <div class="col-md-3">
                                            <div class="input-group">
                <span class="input-group-addon">
                    <b>Precio 1</b> &nbsp;
                    <input type="radio" checked="" value="1" name="producto_opcionprecio" id="producto_opcionprecio1"></span>
                                                <input name="producto_precio1" type="text" class="form-control requerido validador_numero5 usd5" placeholder="" id="producto_precio1" disabled="disabled">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="input-group">
                <span class="input-group-addon">
                    <b>Precio 2</b> &nbsp;
                <input type="radio" value="2" name="producto_opcionprecio" id="producto_opcionprecio2">
                </span>
                                                <input name="producto_precio2" type="text" class="form-control validador_numero5 usd5" placeholder="" id="producto_precio2" disabled="disabled">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="input-group">
                <span class="input-group-addon">
                    <b>Precio 3</b> &nbsp;
                <input type="radio" value="3" name="producto_opcionprecio" id="producto_opcionprecio">
                </span>
                                                <input name="producto_precio3" type="text" class="form-control validador_numero5 usd5" placeholder="" id="producto_precio3" disabled="disabled">
                                            </div>
                                        </div>


                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <div class="col-md-1">
                                            <label class="form-control-label" for="l0">Nombre</label>
                                        </div>
                                        <div class="col-md-5">
            <textarea class="form-control requerido validador_texto300" name="producto_nombre" id="producto_nombre" disabled="disabled">
            </textarea>
                                        </div>
                                        <div class="col-md-1">
                                            <label class="form-control-label" for="l0">Descripción</label>
                                        </div>
                                        <div class="col-md-5">
             <textarea class="form-control validador_texto300" name="producto_descripcion" id="producto_descripcion" disabled="disabled">
            </textarea>
                                        </div>


                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <div class="col-md-1">
                                            <label class="form-control-label" for="l0">Tipo Producto</label>
                                        </div>
                                        <div class="col-md-1">

                                            <select class="form-control requerido" name="producto_tipoproducto" id="producto_tipoproducto" disabled="disabled">
                                                <option value="1" selected="">Bien</option>
                                                <option value="2">Servicio</option>
                                            </select>
                                        </div>
                                        <div class="col-md-1">
                                            <label class="form-control-label" for="l0">Tipo Iva</label>
                                        </div>
                                        <div class="col-md-1">
                                            <select class="form-control requerido" name="producto_tipoiva" id="producto_tipoiva" disabled="disabled">
                                                <option value="2" selected=""> 12% </option>
                                                <option value="0"> 0% </option>
                                                <option value="6"> No Objeto de Impuesto </option>
                                                <option value="7"> Exento de Iva </option>
                                            </select>
                                        </div>
                                        <div class="col-md-1">
                                            <label class="form-control-label" for="l0">ICE</label>
                                        </div>
                                        <div class="col-md-2">
                                            <select class="form-control" name="producto_tipoice" id="producto_tipoice" disabled="disabled"><option value="0">Seleccione Ice</option><option value="3011">3011 - Cigarrillos rubio </option><option value="3021">3021 - Cigarrillos negros </option><option value="3023">3023 - Productos del tabaco y suced&nbsp;n...</option><option value="3031">3031 - Bebidas alcoh¢licas, distintas...</option><option value="3041">3041 - Cerveza Industrial </option><option value="3043">3043 - Cerveza artesanal</option><option value="3053">3053 - Bebidas Gaseosas con alto cont...</option><option value="3054">3054 - Bebidas Gaseosas con bajo cont...</option><option value="3072">3072 - Camionetas, furgonetas, camion...</option><option value="3073">3073 - Veh¡culos motorizados cuyo pre...</option><option value="3074">3074 - Veh¡culos motorizados, excepto...</option><option value="3075">3075 - Veh¡culos motorizados, cuyo pr...</option><option value="3077">3077 - Veh¡culos motorizados, cuyo pr...</option><option value="3078">3078 - Veh¡culos motorizados cuyo pre...</option><option value="3079">3079 - Veh¡culos motorizados cuyo pre...</option><option value="3080">3080 - Veh¡culos motorizados cuyo pre...</option><option value="3081">3081 - Aviones, avionetas y helic¢pte...</option><option value="3092">3092 - Servicios de televisi¢n pagada</option><option value="3093">3093 - Servicios de Telefon¡a </option><option value="3101">3101 - Bebidas energizantes</option><option value="3111">3111 - Bebidas no alcoh¢licas </option><option value="3171">3171 - Veh¡culos h¡bridos o el‚ctrico...</option><option value="3172">3172 - Veh¡culos h¡bridos o el‚ctrico...</option><option value="3173">3173 - Veh¡culos h¡bridos o el‚ctrico...</option><option value="3174">3174 - Veh¡culos h¡bridos o el‚ctrico...</option><option value="3175">3175 - Veh¡culos h¡bridos o el‚ctrico...</option><option value="3176">3176 - Veh¡culos h¡bridos o el‚ctrico...</option><option value="3531">3531 - Bebidas  alcoh¢licas SENAE</option><option value="3541">3541 - Cerveza Industrial SENAE</option><option value="3542">3542 - Cigarrillos rubio SENAE</option><option value="3543">3543 - Cigarrillos negros SENAE</option><option value="3545">3545 - Cerveza artesanal SENAE</option><option value="3552">3552 - Bebidas Gaseosas con alto cont...</option><option value="3553">3553 - Bebidas Gaseosas con bajo cont...</option><option value="3601">3601 - Bebidas energizantes SENAE</option><option value="3602">3602 - Bebidas no alcoh¢licas SENAE</option><option value="3610">3610 - Perfumes y aguas de tocador</option><option value="3620">3620 - Videojuegos </option><option value="3630">3630 - Armas de fuego, armas deportiv...</option><option value="3640">3640 - Focos incandescentes excepto a...</option><option value="3650">3650 - Servicios de casinos, salas de...</option><option value="3660">3660 - Las cuotas, membres¡as, afilia...</option><option value="3670">3670 - Cocinas, calefones y otros de...</option><option value="3680">3680 - Ice Fundas Plásticas</option><option value="3770">3770 - Cocinas, calefones y otros de...</option></select>
                                        </div>
                                        <div class="col-md-1">
                                            <label class="form-control-label" for="l0">Porc Ice</label>
                                        </div>
                                        <div class="col-md-1">
                                            <input name="producto_porcice" type="number" class="form-control" placeholder="0" id="producto_porcice" disabled="disabled">
                                        </div>
                                        <div class="col-md-1">
                                            <label class="form-control-label" for="l0">Irbpnr</label>
                                        </div>
                                        <div class="col-md-2">
                                            <select class="form-control" name="producto_irbpnr" id="producto_irbpnr" disabled="disabled"><option value="0">Seleccione Irbpnr</option><option value="1">5001 - Botellas Plásticas No Retornab...</option></select>
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-1">
                                            <label class="form-control-label" for="l0">Costo</label>
                                        </div>
                                        <div class="col-md-1">
                                            <input name="producto_costo" type="text" class="form-control validador_numero5" placeholder="" id="producto_costo" disabled="disabled">
                                        </div>
                                        <div class="col-md-1">
                                            <label class="form-control-label" for="l0">Stock</label>
                                        </div>
                                        <div class="col-md-1">
                                            <select name="producto_stock" class="form-control" id="producto_stock" disabled="disabled">
                                                <option value="1">SI</option>
                                                <option value="0">NO</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="input-group">
                <span class="input-group-addon">
                    <b>#</b> &nbsp;
                </span>
                                                <input name="producto_stock_real" type="number" class="form-control" value="-1" placeholder="-1" id="producto_stock_real" disabled="disabled">
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <label class="form-control-label" for="l0">Bodega</label>
                                        </div>
                                        <div class="col-md-2">
                                            <select class="form-control" name="producto_bodega" id="producto_bodega" disabled="disabled"><option value="1">Principal</option><option value="27">Prueba bodega</option><option value="28">oTRA BODEGA</option><option value="29">Keyla</option><option value="30">Nadia Perez</option><option value="31">Bodega Manta</option></select>
                                        </div>
                                        <div class="col-md-1">
                                            <label class="form-control-label" for="l0">Categoría</label>
                                        </div>
                                        <div class="col-md-2">
                                            <select class="form-control" name="producto_categoria" id="producto_categoria" disabled="disabled"><option selected="true" value="1">Principal</option></select>
                                        </div>
                                    </div>
                                </div>



                            </div>
                            <div class="modal-footer">

                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button type="button" class="btn btn-danger " data-dismiss="modal">Cerrar
                                            </button>
                                            <button type="button" id="botonguardarproductoyanadirarticulo" class="btn btn-primary" disabled="disabled">
                                                Guardar Producto y Añadir al Carro
                                            </button>
                                            <button type="button" id="botonanadiralcarrito" class="btn btn-primary"> Añadir
                                                y
                                                Continuar
                                            </button>
                                            <button type="button" id="botonanadiralcarritoycerrar" class="btn btn-success">
                                                Añadir y
                                                Cerrar
                                            </button>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-9">


                                        </div>
                                        <div class="col-md-1" style="font-size: 15px;">
                                            Cantidad
                                        </div>
                                        <div class="col-md-2">
                                            <input name="cantidad_carro" type="text" class="form-control validador_numero5 usd5" placeholder="" id="cantidad_carro" value="1">
                                        </div>
                                    </div>

                                </div>


                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
            </div>
            <div class="box-body">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table">
                            <thead style="width:100%;">
                            <tr>
                                <th style="width:130px;">Código</th>
                                <th>Nombre</th>
                                <th>Detalle</th>
                                <th style="width:70px;">Cantidad</th>
                                <th style="width:90px;">P.Unitario</th>
                                <th style="width:50px;">%Desc</th>
                                <th style="width:65px;">Desc</th>
                                <th style="width:75px;">Subtotal</th>
                                <th style="width:35px;">Ice</th>
                                <th style="width:75px;">Iva</th>
                                <th style="width:90px;">Total</th>
                                <th style="width:20px;"><i class="fa fa-times-circle anadir"></i></th>
                            </tr>

                            </thead>
                            <tbody id="detalleventa">


                            </tbody>
                        </table>
                    </div>
                </div>



            </div>


        </div>


        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Forma de Pago</h3>
                        <button type="button" class="btn  btn-primary btn-sm flat" onclick="anadir_formapago()">
                            <i class="fa fa-plus"></i> Añadir
                        </button>
                    </div>
                    <div class="box-body">
                        <table>
                            <tbody class="tabladatosformadepago">
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <b>Forma de Pago</b>
                                        </div>
                                        <div class="col-md-8">
                                            <select id="formadepago" class="form-control" name="formadepago">
                                                <option value="01">Sin utilizacion del sistema financiero</option>
                                                <option value="16">Tarjetas de Debito</option>
                                                <option value="17">Dinero Electronico</option>
                                                <option value="18">Tarjeta Prepago</option>
                                                <option value="19">Tarjeta de Credito</option>
                                                <option selected="" value="20">Otros con Utilizacion del Sistema Financiero</option>
                                                <option value="21">Endoso de Titulos</option>
                                                <option value="15">COMPENSACIÓN DE DEUDAS</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <b>Plazo</b>
                                        </div>

                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input id="plazo" name="plazo" type="number" class="form-control" placeholder="Ejm: 30">
                                                </div>
                                                <div class="col-md-6">
                                                    <select id="tiempo" class="form-control" name="tiempo">
                                                        <option selected="" value="ninguno">ninguno</option>
                                                        <option value="dias">Dias</option>
                                                        <option value="meses">Meses</option>
                                                        <option value="anios">Años</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <b>Valor</b>
                                        </div>
                                        <div class="col-md-8">
                                            <input class="form-control usd2" value="0.00" type="text" placeholder="" id="totalformapago">
                                        </div>
                                    </div>
                                </td>
                                <td><a id="eliminar_formapago"><i class="fa fa-times-circle anadir"></i></a></td>
                            </tr>
                            </tbody>

                        </table>


                    </div>
                </div>

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Datos adicionales (Opcional) </h3>
                        <button type="button" class="btn  btn-primary btn-sm flat" data-toggle="modal" data-target="#modal-default1">
                            <i class="fa fa-plus"></i> Añadir
                        </button>
                    </div>
                    <div class="box-body ">
                        <table class=" table ">
                            <thead>
                            <tr><th>Nombre</th>
                                <th>Detalle</th>
                            </tr></thead>
                            <tbody id="tabladatosadicianales" class="tabladatosadicianales">

                            </tbody>
                        </table>
                    </div>
                    <div class="modal fade" id="modal-default1" style="display: none;">
                        <div class="modal-dialog ">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span></button>
                                    <h4 class="modal-title">Datos Adicionales</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-2"><b>Nombre</b></div>
                                        <div class="col-md-4">
                                            <input id="nombre_adicional" name="nombre_adicional" type="text" class="form-control validador_texto300" placeholder="Ejm: Orden">
                                        </div>
                                        <div class="col-md-2"><b>Descripción</b></div>
                                        <div class="col-md-4">
                                            <input id="detalle_adicional" name="detalle_adicional" type="text" class="form-control validador_texto300" placeholder="Ejm: ABCD123">
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>

                                    <button class="btn  btn-primary " id="botonanadiracional"> Añadir Adicional</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box">
                    <div class="box-body">
                        <div class="col-md-12">
                            <div class="col-md-6"><b>Guia de Remisioón (Opcional)</b></div>
                            <div class="col-md-6"><input type="text" name="guia_remision" id="guia_remision" class="form-control validador_secuencialdocumento" placeholder="___-___-_________"></div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Totales</h3>
                    </div>
                    <div class="box-body">
                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Subtotal sin
                                        Impuestos</label></div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_subtotal_sinimpuestos"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group row price">


                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Subtotal Iva
                                        12%</label></div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_subtotal_iva12"></div>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Subtotal 0%</label>
                                </div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_subtotal_ivacero"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Subtotal no objeto
                                        de iva</label></div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_subtotal_noobjeto"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Subtotal Exento de
                                        iva</label></div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_subtotal_exento"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Total
                                        descuento</label></div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_descuento"></div>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Valor ICE</label>
                                </div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_ice"></div>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Valor irbpnr</label>
                                </div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_irbpnr"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Iva 12%</label>
                                </div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_iva12"></div>
                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Propina 10%</label>
                                    <input type="checkbox" id="propina" name="propina" value="propina" onclick="propina()">
                                </div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_propina"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group row price">
                                <div class="col-md-6"><label class="form-control-label tvalor" for="l0">Valor Total</label>
                                </div>
                                <div class="col-md-6"><input class="valorcito" disabled="" value="0.00" type="text" placeholder="" id="totales_valortotal"></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        <input type="checkbox" id="reembolso" name="reembolso" value="reembolso" onclick="opcion_reembolso()"> Aplica Reembolso de Gastos

        <div class="opcion_reembolso" style="display: none;">
            <div class="row">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Reembolso de Gastos</h3>
                        <button type="button" class="btn  btn-primary btn-sm flat" data-toggle="modal" data-target="#modal-reembolso">
                            <i class="fa fa-plus"></i> Añadir Reembolso
                        </button>
                    </div>

                    <div class="box-body ">
                        <table class=" table ">
                            <thead>
                            <tr>
                                <th>Proveedor</th>
                                <th>Tipo Comprobante</th>
                                <th>Secuencial</th>
                                <th>Fecha emision</th>
                                <th>Autorizacion</th>
                                <th>Base Iva 0%</th>
                                <th>Base Iva 12%</th>
                                <th>Valor Ice</th>
                                <th>Total</th>
                                <th>Acciones</th>

                            </tr>

                            </thead>
                            <tbody id="tablareembolsos" class="tablareembolsos">

                            </tbody>

                        </table>

                        <div class="col-md-6">
                            <div class="col-md-6"><b>Total Reembolso</b></div>
                            <div class="col-md-6"><input type="text" class="valorcito" name="totales_reembolso" id="totales_reembolso" disabled=""></div>
                        </div>



                    </div>

                    <div class="modal fade" id="modal-reembolso" style="display: none;">
                        <div class="modal-dialog ">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span></button>
                                    <h4 class="modal-title">Añadir Reembolso</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-4"><b>Proveedor</b></div>
                                        <div class="col-md-8">
                                            <div class="input-group">
                                                <input id="criterioproveedor" name="criterioproveedor" class="form-control" type="text" value="" placeholder="Ingrese su Busqueda">
                                                <span class="input-group-btn">
                            <button id="botonbuscarproveedorextraboton" type="button" class="btn btn-primary btn-flat"><i class="fa  fa-search"></i></button>
                          </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            Seleccione un Resultado
                                        </div>
                                        <div class="col-md-8">
                                            <select class="form-control" id="proveedores">
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            Nombre
                                        </div>
                                        <div class="col-md-8">
                                            <input type="hidden" name="proveedor_id_proveedor" id="proveedor_id_proveedor">
                                            <input name="proveedor_nombre" type="text" class="form-control requerido " placeholder="9999999999999" id="proveedor_nombre" disabled="disabled">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            Ruc
                                        </div>
                                        <div class="col-md-8">
                                            <input name="proveedor_ruc" type="text" class="form-control requerido " placeholder="9999999999999" id="proveedor_ruc" disabled="disabled">
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="form-control-label" for="l0">Tipo de Documento</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select id="proveedor_tipodocumento" name="proveedor_tipodocumento" class="form-control">
                                                <option value="01" selected="selected">Factura</option>
                                                <option value="03">Liq de Compras</option>
                                                <option value="05">Nota de Debito</option>
                                                <option value="19">COMPROBANTE DE PAGO APORTES</option>
                                                <option value="11">Pasajes emitidos por empresas de aviación</option>
                                                <option value="12">Documentos emitidos por instituciones financieras</option>
                                                <option value="13">Documentos emitidos por compañias de seguros</option>
                                                <option value="15">Comprobantes de venta emitidos en el exterior</option>
                                                <option value="18">Documentos autorizados en ventas excepto ND y NC</option>
                                                <option value="21">Carta de porte aereo</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="form-control-label" for="l0">Nro.Comprobante:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control validador_secuencialdocumento" name="proveedor_documento_sustento" id="proveedor_documento_sustento" placeholder="___-___-_________">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="form-control-label" for="l0">Autorización:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control " name="proveedor_autorizacion" id="proveedor_autorizacion">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="form-control-label" for="l0">Fecha de Emision</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="proveedor_documento_fecha" id="proveedor_documento_fecha" value="2021-04-18">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-2">
                                            <label class="form-control-label" for="l0">ICE</label>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="col-md-4">
                                                Codigo
                                                <select class="form-control" name="reembolso_tipoice" id="reembolso_tipoice"><option value="0">Seleccione Ice</option><option value="3011">3011 - Cigarrillos rubio </option><option value="3021">3021 - Cigarrillos negros </option><option value="3023">3023 - Productos del tabaco y suced&nbsp;n...</option><option value="3031">3031 - Bebidas alcoh¢licas, distintas...</option><option value="3041">3041 - Cerveza Industrial </option><option value="3043">3043 - Cerveza artesanal</option><option value="3053">3053 - Bebidas Gaseosas con alto cont...</option><option value="3054">3054 - Bebidas Gaseosas con bajo cont...</option><option value="3072">3072 - Camionetas, furgonetas, camion...</option><option value="3073">3073 - Veh¡culos motorizados cuyo pre...</option><option value="3074">3074 - Veh¡culos motorizados, excepto...</option><option value="3075">3075 - Veh¡culos motorizados, cuyo pr...</option><option value="3077">3077 - Veh¡culos motorizados, cuyo pr...</option><option value="3078">3078 - Veh¡culos motorizados cuyo pre...</option><option value="3079">3079 - Veh¡culos motorizados cuyo pre...</option><option value="3080">3080 - Veh¡culos motorizados cuyo pre...</option><option value="3081">3081 - Aviones, avionetas y helic¢pte...</option><option value="3092">3092 - Servicios de televisi¢n pagada</option><option value="3093">3093 - Servicios de Telefon¡a </option><option value="3101">3101 - Bebidas energizantes</option><option value="3111">3111 - Bebidas no alcoh¢licas </option><option value="3171">3171 - Veh¡culos h¡bridos o el‚ctrico...</option><option value="3172">3172 - Veh¡culos h¡bridos o el‚ctrico...</option><option value="3173">3173 - Veh¡culos h¡bridos o el‚ctrico...</option><option value="3174">3174 - Veh¡culos h¡bridos o el‚ctrico...</option><option value="3175">3175 - Veh¡culos h¡bridos o el‚ctrico...</option><option value="3176">3176 - Veh¡culos h¡bridos o el‚ctrico...</option><option value="3531">3531 - Bebidas  alcoh¢licas SENAE</option><option value="3541">3541 - Cerveza Industrial SENAE</option><option value="3542">3542 - Cigarrillos rubio SENAE</option><option value="3543">3543 - Cigarrillos negros SENAE</option><option value="3545">3545 - Cerveza artesanal SENAE</option><option value="3552">3552 - Bebidas Gaseosas con alto cont...</option><option value="3553">3553 - Bebidas Gaseosas con bajo cont...</option><option value="3601">3601 - Bebidas energizantes SENAE</option><option value="3602">3602 - Bebidas no alcoh¢licas SENAE</option><option value="3610">3610 - Perfumes y aguas de tocador</option><option value="3620">3620 - Videojuegos </option><option value="3630">3630 - Armas de fuego, armas deportiv...</option><option value="3640">3640 - Focos incandescentes excepto a...</option><option value="3650">3650 - Servicios de casinos, salas de...</option><option value="3660">3660 - Las cuotas, membres¡as, afilia...</option><option value="3670">3670 - Cocinas, calefones y otros de...</option><option value="3680">3680 - Ice Fundas Plásticas</option><option value="3770">3770 - Cocinas, calefones y otros de...</option></select>
                                            </div>
                                            <div class="col-md-2">
                                                Porc
                                                <input type="number" class="form-control porc" name="proveedor_porcice" id="proveedor_porcice" value="" onchange="actualizaranadirreembolso()">

                                            </div>
                                            <div class="col-md-3">
                                                Base
                                                <input type="text" class="form-control validador_numero2 usd2" name="proveedor_baseice" id="proveedor_baseice" value="" onchange="actualizaranadirreembolso()">
                                            </div>
                                            <div class="col-md-3">
                                                Total ice
                                                <input type="text" class="form-control validador_numero2 usd2" name="proveedor_totalice" id="proveedor_totalice" value="" disabled="">
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="form-control-label" for="l0">Base Iva 0%</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control validador_numero2 usd2" name="proveedor_baseiva0" id="proveedor_baseiva0" value="" onchange="actualizaranadirreembolso()">
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="form-control-label" for="l0">Base Iva 12%</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control validador_numero2 usd2" name="proveedor_baseiva12" id="proveedor_baseiva12" value="" onchange="actualizaranadirreembolso()">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="form-control-label" for="l0">Total</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control validador_numero2 usd2" name="proveedor_total" id="proveedor_total" value="" disabled="">
                                        </div>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>

                                    <button class="btn  btn-primary " id="botonanadirreembolso"> Añadir Reembolso</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>



        <center>
            <button type="button" class="btn btn-primary " onclick="enviar()"><i class="fa fa-save"></i> Guardar, Firmar y
                Enviar
            </button>
        </center>


    </section>


    <script type="text/javascript">


    function prueba()
    {

        var criterio = $('#criteriocliente').val();
        if(criterio!=""){
            buscarcliente();
        }else{
            aviso("error","Debe Ingresar por lo menos un digito para buscar.","","");
        }
        //console.log('Hola');
    }


    function buscarcliente() {
        $("#tipo_evento_cliente").val("");
        var criterio = $('#criteriocliente').val();
        bloquearcliente();
        $.ajax({
            type: 'get',
            url: "{{route('ajaxSearch')}}",
            data: {criterio: criterio},
            success: function (resp) {
                if (resp.respuesta == true) {
                    $('#clientes').empty();
                    $(resp.datos).each(function (i, v) {
                        $('#clientes').append(' <option value=' + v.id + '>' + v.nombrerazonsocial + '</option>');
                    });

                    buscarsucursalcliente();
                }
            },
            err:function (){
                console.log('');
            }
        })
    }


    function bloquearcliente() {

        $('#tipoidentificacion').attr('disabled', 'disabled');
        $("#identificacion").attr('disabled', 'disabled');
        $("#nombrerazonsocial").attr('disabled', 'disabled');
        $("#direccion").attr('disabled', 'disabled');
        $("#telefono").attr('disabled', 'disabled');
        $("#celular").attr('disabled', 'disabled');
        $("#correo").attr('disabled', 'disabled');
        $('#sucursal').attr('disabled', 'disabled');
        $('#codigosucursal').attr('disabled', 'disabled');
        $('#provincia').attr('disabled', 'disabled');
        $('#ciudad').attr('disabled', 'disabled');
    }



    /*$('#botonbuscarextraboton').click(function () {
        var criterio = $('#criteriocliente').val();
        if(criterio!=""){
            buscarcliente();
        }else{
            aviso("error","Debe Ingresar por lo menos un digito para buscar.","","");
        }
    });*/




    </script>
@endsection

