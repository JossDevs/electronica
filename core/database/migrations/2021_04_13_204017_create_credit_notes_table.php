<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCreditNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_notes', function (Blueprint $table) {
            $table->string('uuid', 36)->primary();
            $table->string('client_id', 36)->index('creditnote_client_id_foreign');
            $table->string('branch_id', 36)->index('creditnote_branch_id_foreign');
            $table->string('company_id', 36)->index('creditnote_company_id_foreign');
            $table->string('establishment_id', 36)->index('creditnote_establishment_id_foreign');
            $table->string('emissionpoint_id', 36)->index('creditnote_emissionpoint_id_foreign');
            $table->string('user_id', 36)->index('creditnote_user_id_foreign');
            $table->string('seller_id', 36)->index('creditnote_seller_id_foreign');
            $table->date('date');
            $table->string('number');
            $table->string('environment');
            $table->double('subtotal_withouttax', 15, 8);
            $table->double('subtotal_tax12', 15, 8);
            $table->double('subtotal_cerotax', 15, 8);
            $table->double('subtotal_exempttax', 15, 8);
            $table->double('subtotal_noobject', 15, 8);
            $table->double('total_ice', 15, 8);
            $table->double('total_iva12', 15, 8);
            $table->double('total_irbpnr', 15, 8);
            $table->double('total_propine', 15, 8);
            $table->double('total_totalvalue', 15, 8);
            $table->string('support_receipt_type');
            $table->string('support_receipt_reason');
            $table->string('support_receipt_sequencial');
            $table->string('support_receipt_reason');       
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_notes');
    }
}
