<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHoardingItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hoarding_item', function (Blueprint $table) {
            $table->string('uuid', 36)->primary();
            $table->string('warehouse_id', 36)->index('hoarding_warehouse_id_foreign');
            $table->string('code');
            $table->string('auxiliar_code');
            $table->string('name');
            $table->string('description');
            $table->integer('number');
            $table->string('quantity');
            $table->double('discount', 15, 8);
            $table->double('price', 15, 8);
            $table->string('code_ice', 15, 8);
            $table->string('porc_ice', 15, 8);
            $table->double('discount', 15, 8);
            $table->double('ice', 15, 8);
            $table->double('iva_type', 15, 8);
            $table->double('total', 15, 8);
            $table->double('subtotal', 15, 8);
          

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hoarding_item');
    }
}
