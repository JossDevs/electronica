<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDebitNotesItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debit_notes_item', function (Blueprint $table) {
            $table->string('uuid', 36)->primary();
            $table->string('client_id', 36)->index('creditnoteitem_client_id_foreign');
            $table->string('branch_id', 36)->index('creditnoteitem_branch_id_foreign');
            $table->string('company_id', 36)->index('creditnoteitem_company_id_foreign');
            $table->string('code');
            $table->string('auxiliar_code');
            $table->string('name');
            $table->string('description');
            $table->integer('quantity');
            $table->double('price');
            $table->string('code_ice');
            $table->double('price');
            $table->integer('porc_ice');
            $table->double('ice');
            $table->double('total');
            $table->double('subtotal');
            $table->double('total');
            $table->string('iva_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debit_notes_item');
    }
}
