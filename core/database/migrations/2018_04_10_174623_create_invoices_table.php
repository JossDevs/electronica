<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInvoicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('invoices', function(Blueprint $table)
		{
			$table->string('uuid', 36)->primary();
			$table->string('client_id', 36)->index('invoices_client_id_foreign');
			$table->string('number')->unique();
			$table->date('invoice_date');
			$table->date('due_date');
			$table->integer('status');
			$table->float('discount');
			$table->enum('discount_mode', array('0','1'))->default('1');
			$table->text('terms', 65535);
			$table->text('notes', 65535);
			$table->string('number');
			$table->integer('branch_id');
			$table->integer('company_id');
			$table->integer('user_id');
			$table->integer('seller_id');
			$table->integer('establishment_id');
			$table->integer('emissionpoint_id');
			$table->string('sequential');
			$table->string('environment');
			$table->double('subtotal_withouttax', 15, 8);
			$table->double('subtotal_tax12', 15, 8);
			$table->double('subtotal_cerotax', 15, 8);
			$table->double('subtotal_exempttax', 15, 8);
			$table->double('subtotal_noobject', 15, 8);
			$table->double('total_ice', 15, 8);
			$table->double('total_iva12', 15, 8);
			$table->double('total_irbpnr', 15, 8);
			$table->double('total_propine', 15, 8);
			$table->double('total_totalvalue', 15, 8);
			$table->string('currency');
			$table->boolean('recurring');
			$table->boolean('recurring_cycle');
			$table->timestamps();
		});
        // then I add the increment_num "manually"
        DB::statement('ALTER Table invoices add increment_num INTEGER NOT NULL UNIQUE AUTO_INCREMENT;');
	}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('invoices');
	}

}
